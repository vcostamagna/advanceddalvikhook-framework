### Advanced dalvik dynamic instrumentation ###


Il framework ADDI è composto da due side, uno nativo e uno Java. 
La parte nativa gestisce l'instrumentazione di Dalvik e gli hook.
La parte Java contiene la definizione degli hook, il codice da eseguire quando scatta un hook e le classi che permettono la creazione dei log e dei report di esecuzione. 
Il codice degli hook puó interagire con il codice nativo utilizzando la libreria AddiApi (includere nel buildpath il jar).
Questo progetto prende spunto dalla tecnica divulgata da Collin Mulliner, l'instrumentazione di DalvikVM, e implementata nel framework DDI. Collin utilizza JNI per gestire le varie fasi degli hook (scatto,patch code, original call) peró  gli hook non vengono gestiti in maniera thread-safe (inoltre il codice di DDI é da considerare un (ottimo) POC, non offre gestione degli hook, etc...). Nel  caso in cui piú di un thread esegua una chiamata allo stesso metodo (quello hookato), non é garantito che tutte le chiamate vengano intercettate dal hook, cioé non gestisce la concorrenza delle chiamate allo stesso metodo. 
Nel framework DDI l hook viene impostato modificando la referenza della classe, che contiene il metodo, in memoria e per eseguire la chiamata originale vengono ripristinati i  valori originari che sono stati modificati per impostare l hook, successivamente questi vengono nuovamente modificati. (consulare il codice di DDI, la finestra non thread-safe si crea tra la chiamata alla funzione dalvik_prepare() e dalvik_postcall()). 
ADDI implementa il meccanismo di instrumentazione di DalvikVM in modo thread-safe e offre un'interfaccia di gestione dinamica per modificare/aggiungere hook a run-time od avviare l'esecuzione di moduli in un nuovo thread nativo.
ADDI crea una copia del metodo originale prima che questo venga modificato per impostare l'hook, tale copia verrá utilizzata per eseguire la chiamata originale senza peró utilizzare JNI ma sfruttando le funzioni di DalvikVM.
In questo modo la referenza in memoria é sempre quella che punta all hook e per la chiamta originale si utilizza la copia. Non si puó usare JNI per eseguire la chiamata originale perché per invocare i metodi virtuali JNI utilizza il lookup del metodo utilizzando la vable, ottenendo sempre la referenza al metodo che contiene l'hook.

### Struttura File ###

1. dalvikhook/jni ---> framework (crea lib statica)
	1. +dalvikhook/jni/dexstuff.c ---> instrumentazione dalvik, dex support
	2. +dalvikhook/jni/dalvik_hook.c ---> hook 

2. examples/
	1. +dynsec/jni	---> crea il .so da caricare in memoria
	2. +HooksCode/ ---> Progetto Eclipse dei moduli java
	3. +HooksCode/bin/classes.dex ---> DEX con il codice di controllo 

3. AddiApi/  ---> API framework, includere il .jar nel buildpath del codice di controllo Java

4. scripts/ ---> helper scripts. I .sh citati sotto sono in questa dir

5. parser.py ---> tool python per generare il grafo delle chiamate usando gli stacktraces salvati a run-time (usa networkx)


### Dipendenze ###

- root sul device 
- adbi https://github.com/crmulliner/adbi
- android ndk (per compilare)
- busybox in PATH: mknod e simili..

### Installazione ###

0. Compilare Adbi e copiare il binario "hijack" nella cartella "scripts"
1. cd scripts; ./pushfiles.sh
2. copiare sul dispositivo anche il dex delle API rinominandolo come apiclasses.dex e il dex del codice di controllo lasciando il nome a classes.dex

### Esecuzione ###

## su device in cui é presente SELinux, prima di eseguire il codice disabilitatelo con il comando setenforce  0 ##

3. adb shell (da root é piú bello)
4. cd /data/local/tmp;
5. chmod 775 initenv.sh; % solo una volta per ogni riavvio
6. sh initenv.sh  % solo una volta per ogni riavvio
7. sh runhijack.sh  % seguire l' help

### Log ###

Ad ogni run viene creata, dentro la directory /data/local/tmp/dynsec, una cartella con il nome del pid dell'applicazione al cui interno sono contenuti il log del framework e le pipe per la comunicazione.

### Compilazione e Tips ###

Per compilare utilizzare il toolchain di android-ndk, impostata la path si puó usare lo script compile.sh
Se tra un run e un altro si vuole cambiare il file .dex, ricordarsi di eliminare il vecchio in /data/dalvik-cache.
impostare checkjni e checkmalloc con setprop per debug in caso di anomalie.
Per ulteriori info guardare nella cartella "docs/"




