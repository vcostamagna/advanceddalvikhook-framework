package insecureapp.tesi.org.insecureapp;

import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;

import com.robotium.solo.Solo;

import static android.os.SystemClock.currentThreadTimeMillis;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends ActivityInstrumentationTestCase2<MyActivity> {
    private Solo solo;
    private static int NUMTEST = 1;
    private String[] myButtons = {"SSL", "Fswrite","DynReceiver", "Reflection", "Hash", "Crypto"};
    private Class[] myActivities = {SSLActivity.class, FSwriteActivity.class, dynamicReceiverActivity.class,
            reflectActivity.class, hashActivity.class, cryptoActivity.class};
    public ApplicationTest() {
        super(MyActivity.class);
    }

    public void setUp() throws Exception {
        solo = new Solo(getInstrumentation(), getActivity());
    }
    public void testFoo() throws Exception {
        //long startTime = currentThreadTimeMillis();
        for(int i = 0;i<NUMTEST;i++) {
            for(int k=1;k<myButtons.length;k++) {
                solo.clickOnButton(myButtons[k]);
                solo.waitForActivity(myActivities[k]);
                solo.waitForText("FINITO", 0, 10000000);
                solo.goBack();
            }
        }
        /*
        long endTime = currentThreadTimeMillis();
        long execTime = (endTime-startTime)/1000;
        Log.d("CIPPA", "%%%%%%%%%%%%%%%%%%%%%%% tempo di esecuzione in secondi: " + endTime);
        Timing.saveTime("myActivity",endTime);
        */
    }
    public void NOtestPreferenceIsSaved() throws Exception {
        //long startTime = currentThreadTimeMillis();
        for(int i = 0;i<0;i++) {
            for(int k =1;k<myButtons.length;k++) {
                solo.clickOnButton(myButtons[k]);
                solo.waitForActivity(myActivities[k]);
                solo.waitForText("FINITO", 0, 10000000);
                solo.goBack();
            }
        }
        //long endTime = currentThreadTimeMillis();
        //long execTime = (endTime-startTime)/1000;
        //Log.d("CIPPA", "%%%%%%%%%%%%%%%%%%%%%%% tempo di esecuzione in secondi: " + endTime);
        //Timing.saveTime("myActivity",endTime);
    }

    @Override
    public void tearDown() throws Exception {
        solo.finishOpenedActivities();
    }
}