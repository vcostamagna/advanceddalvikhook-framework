package insecureapp.tesi.org.insecureapp;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;


public class FSwriteActivity extends Activity {
    public final String fileName = "secret.txt";
    public final String text = "THIS IS MY CREDIT CARD NUMBER: XXXX";

    FileOutputStream outputStream;
    FileOutputStream outputStream2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fswrite2);
        TextView text = (TextView) findViewById(R.id.maintext);
        text.setText("Scrivo file: ");
        Timing.startTime();
        writeFile();
        writeExternalFile();
        Timing.endTime();
        Timing.saveTime("FSwriteactivity");
        text.append("FINITO");
    }

    public void writeFile(){
        try {
            outputStream = openFileOutput(fileName, Context.MODE_WORLD_READABLE);
            outputStream.write(text.getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void writeExternalFile(){
        File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "mysecret");
        try {
            OutputStream out = new BufferedOutputStream(new FileOutputStream(file));
            out.write(text.getBytes());
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.fswrite, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
