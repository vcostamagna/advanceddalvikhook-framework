package insecureapp.tesi.org.insecureapp;

import android.content.Context;
import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;

import static android.os.SystemClock.currentThreadTimeMillis;

/**
 * Created by sid on 18/09/14.
 */
public class Timing {
    public static int counter = 0;
    public static String FILENAME = "/mnt/sdcard/timing";
    public static double startTime;
    public static double endTime;
    public static double execTime;
    public static double execTimeSec;


    static{
      FILENAME = FILENAME + android.os.Process.myPid()+".log";
    };
    public static int NUMTEST = 10;
    public static Context c = MyApplication.getAppContext();
    //public static FileOutputStream fos = null;
    public static BufferedWriter out = null;
    public static void openFile() throws IOException {
        if(out != null) return;
        out = new BufferedWriter(new FileWriter(FILENAME, true));
    }
    public static void closeFile(){
        if(out == null) return;
        try {
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void header(String s){
        if(out == null){
            try {
                openFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            out.write("!"+s+"\n");
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void endTime(){
        endTime = currentThreadTimeMillis();
        execTime = (endTime-startTime);
        execTimeSec = (endTime-startTime)/1000.0;
        Log.d("CIPPA", "%%%%%%%%%%%%%%%%%%%%%%% start time: "+startTime+" end time: "+endTime+" tempo di esecuzione in secondi: " + execTime);
    }
    public static void startTime(){
        startTime = currentThreadTimeMillis();

    }
    public static void reset(){
        counter = 1;
    }
    public static void saveTime(String s){
        Log.d(MyActivity.TAG,"Salvo log su txt");
        if(out == null){
            try {
                openFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
       // if(counter%NUMTEST == 0)
        //    header(s);
        try {
            //out.write("#"+((counter%NUMTEST)+1)+" ");
            out.write("#"+s+":"+execTime+"\n");
            //out.write(t + "\n");
            out.flush();
            counter++;
        } catch (IOException e) {
            Log.d(MyActivity.TAG,"Errore grave grave");
            e.printStackTrace();
        }
        //closeFile();
    }

}

