package insecureapp.tesi.org.insecureapp;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class hashActivity extends Activity {
    public final String secret = "SEGRETOSEGRETO";
    TextView view = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hash2);
        view = (TextView) findViewById(R.id.maintext);
        Timing.startTime();
        String res = calculateHash();
        Timing.endTime();
        Timing.saveTime("hashActivity");
        view.setText(res+"\n");
        view.append("FINITO");

    }

    public String calculateHash(){
        MessageDigest messageDigest = null;
        String res = null;
        try {
            messageDigest = MessageDigest.getInstance("SHA-256");
            messageDigest.update(secret.getBytes());
            byte[] b = messageDigest.digest();
            //String hash = new String(messageDigest.digest());
            res = StringHelper._getReadableByteArr(b);
            Log.i(MyActivity.TAG, "Calcolato hash: " + res);
            Log.i(MyActivity.TAG, "Calcolato hash: " + b.toString());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return res;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.hash, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
