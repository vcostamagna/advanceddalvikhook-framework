package insecureapp.tesi.org.insecureapp;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.lang.reflect.Field;


public class reflectActivity extends Activity {
    public static TextView text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reflect);
        text = (TextView) findViewById(R.id.maintext);
        text.setText("START\n");
        Timing.startTime();
        useReflection();
        useReflectionErrorCatch();
        try {
            useReflectionErrorThrows();
        } catch (ClassNotFoundException e) {
            System.out.println("dentro on create exception");
            text.append(e.getException().toString());
        }
        Timing.endTime();
        Timing.saveTime("reflectActivity");
        text.append("\nFINITO");
    }
    public static void useReflection(){
        try {
            Field fieldHasActionBar = Class.forName("android.support.v7.app.ActionBarActivityDelegate").getDeclaredField("mHasActionBar");
        } catch (ClassNotFoundException e) {
            System.out.println("dentro catch exception ANDROID SUPPORT");
            text.append(e.getException().toString());
        } catch (NoSuchFieldException e) {
            System.out.println("dentro catch exception ANDROID SUPPORT");
            text.append(e.getMessage());;
        }
    }
    public static void useReflectionErrorCatch(){
        try {
            Class c = Class.forName("java.lang.CIPPA");
        } catch (ClassNotFoundException e) {
            System.out.println("dentro catch exception CIPPA");
            text.append(e.getException().toString());
        }
    }
    public static void useReflectionErrorThrows() throws ClassNotFoundException {
        Class c = Class.forName("CIPPA");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.reflect, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
