package insecureapp.tesi.org.insecureapp;

import java.io.File;
import java.io.IOException;

import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.provider.ContactsContract;
import android.app.Activity;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import static android.os.SystemClock.*;


public class SSLActivity extends Activity {
    TextView text = null;
    private String result = "";
        private final String TAG = "FSFSFSFSFSFSFSFSFS";
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_ssl);
            text = (TextView) findViewById(R.id.maintext);
            //detectEmulator();
//		testFile();
//		testPackageManager();
            testSSL();
            text.setText("FINITO");
//            testRemoveLocks();
//		testStartActivity();
        }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.ssl, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

        private void testStartActivity(){
            startActivity(new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS));
        }

        private void testRemoveLocks() {
            Intent intent = new Intent();
            intent.setComponent(new ComponentName("com.android.settings", "com.android.settings.ChooseLockGeneric"));
            intent.putExtra("confirm_credentials", false);
            intent.putExtra("lockscreen.password_type",0);
            intent.setFlags(intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }

        private void testSSL(){
            //SSLRight test1 = new SSLRight();
            //test1.execute("https://www.unito.it");

            SSLRight test2 = new SSLRight();
            test2.execute("https://www.pcwebshop.co.uk/");

            //SSLWrong test3 = new SSLWrong();
            //test3.execute("https://www.unito.it");
            SSLWrong test4 = new SSLWrong();
            test4.execute("https://www.pcwebshop.co.uk/");

        }

        private void testPackageManager() {
            PackageManager p = getPackageManager();
            try {
                p.getActivityInfo(getComponentName(), 0);
                Log.i("TEST2", "GetActivityInfo is done");
                p.getPackageInfo(getPackageName(), 0);
                Log.i("TEST2", "getPackageInfo is done");
            } catch (NameNotFoundException e) {
                e.printStackTrace();
            }
//		p.setComponentEnabledSetting(getComponentName(),
//				PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
//				PackageManager.DONT_KILL_APP);
//		Log.i("TEST2", "App icon should disappear");
        }

        private void detectEmulator(){
            Log.i("TEST2", "Build.SDK value: "+ Build.VERSION.SDK);
            Log.i("TEST2", "Build.PRODUCT value: "+ Build.PRODUCT);

            if ("sdk".equals( Build.PRODUCT )) {
                Log.i("TEST", "EQUALS");
            } else{
                Log.i("TEST", "NOT EQUALS");
            }
        }

        private void testFile() {
            File path = Environment.getExternalStorageDirectory();
            File file = new File(path.getAbsolutePath()+"/test");
            Log.i("TEST", "Testfile test: "+file.getAbsolutePath());
            if (!file.exists()) {
                try {
                    Log.i("TEST", "File not exists, creating new file");
                    file.createNewFile();
                    Log.i("TEST", "createNewFile done");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }else{
                Log.i("TEST", "file exist");
            }
        }

        public void readContacts() {
            ContentResolver cr = getContentResolver();
            Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null,
                    null, null);

            if (cur.getCount() > 0) {
                while (cur.moveToNext()) {
                    String id = cur.getString(cur
                            .getColumnIndex(ContactsContract.Contacts._ID));
                    String name = cur.getString(cur
                            .getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                    if (Integer.parseInt(cur.getString(cur
                            .getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                        System.out.println("name : " + name + ", ID : " + id);

                        // get the phone number
                        Cursor pCur = cr.query(
                                ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                                new String[] { id }, null);
                        while (pCur.moveToNext()) {
                            String phone = pCur.getString(pCur
                                    .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                            System.out.println("phone" + phone);
                        }
//					pCur.close();
                    }
                }
            }
        }


    }





















    /**
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ssl);
        new Thread() {
            public void run() {
                try {
                    long startTime = SystemClock.currentThreadTimeMillis();
                    //Debug.startMethodTracing("insecuretest.trace");
                    TrustManager[] trustAllCerts = new TrustManager[] {
                            new X509TrustManager() {
                                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                                    return new X509Certificate[0];
                                }
                                public void checkClientTrusted(
                                        java.security.cert.X509Certificate[] certs, String authType) {
                                }
                                public void checkServerTrusted(
                                        java.security.cert.X509Certificate[] certs, String authType) {
                                }
                            }
                    };

                    // Install the all-trusting trust manager
                    try {
                        SSLContext sc = SSLContext.getInstance("SSL");
                        sc.init(null, trustAllCerts, new java.security.SecureRandom());
                        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
                    } catch (GeneralSecurityException e) {
                        e.printStackTrace();
                    }
                    URL url = new URL("https://www.autistici.org");
                    Log.d(TAG, "[+] Open Connection...");
                    HttpsURLConnection urlConnection = (HttpsURLConnection) url
                            .openConnection();
                    Log.d(TAG, "[+] Get the input stream...");
                    InputStream in = urlConnection.getInputStream();
                    Certificate[] certs = urlConnection.getServerCertificates();
                    BufferedReader reader = new BufferedReader(
                            new InputStreamReader(in));
                    String line = null;
                    Log.d(TAG, "[+] Read all of the return....");
                    while ((line = reader.readLine()) != null) {
                        result += line + "\n";
                    }
                    Log.d(TAG, result);
                    long endTime = SystemClock.currentThreadTimeMillis();
                    long execTime = (endTime-startTime)/1000;
                    Log.d(TAG,"tempo di esecuzione in secondi: "+endTime);
                    Timing.saveTime("SSLTest",endTime);
                    messageHandler.sendEmptyMessage(0);
                } catch (SSLHandshakeException e) {
                    Log.d(TAG, "[-] SSL HANDSHAKE EXCEPTION: " + e.toString());
                    e.printStackTrace();
                }catch (IOException e) {
                    Log.d(TAG, "[-] IO EXCEPTION (HOSTNAME): " + e.toString());
                    Log.d(TAG,
                            "[-] IO EXCEPTION (HOSTNAME)2: "
                                    + e.getLocalizedMessage());
                    Log.d(TAG,
                            "[-] IO EXCEPTION (HOSTNAME)3: " + e.getMessage());
                    e.printStackTrace();
                }catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }
    private Handler messageHandler = new Handler() {

        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            // Get the textview
            TextView text = (TextView) findViewById(R.id.maintext);

            // Set the text
            text.setText(result);
            Log.d(TAG, "[+] SUCCESS");
            Log.d(TAG, "[+] SUCCESS");
            Log.d(TAG, "[+] SUCCESS");
            Toast.makeText(getApplicationContext(), "SUCCESS",
                    Toast.LENGTH_LONG).show();
            //Debug.stopMethodTracing();
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.ssl, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
     **/
