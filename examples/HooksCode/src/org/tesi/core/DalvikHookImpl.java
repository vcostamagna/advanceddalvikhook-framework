package org.tesi.core;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.sid.addi.core.DalvikHook;
import org.sid.addi.core.HookT.HookType;

public class DalvikHookImpl extends DalvikHook{
	public static final String _TAG = "Hooks";
	public static final boolean debug = false;
	public static final String filter = "";
	public DalvikHookImpl(String clname, String method_name, String method_sig, String dex_method, String dex_class,HookType t){
		super(clname, method_name, method_sig, dex_method, dex_class, t );
	}
	public DalvikHookImpl(){
		
	}
}
