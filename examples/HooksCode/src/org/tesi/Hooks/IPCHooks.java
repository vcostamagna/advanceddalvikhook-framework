package org.tesi.Hooks;




import org.tesi.core.DalvikHookImpl;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;

public class IPCHooks extends DalvikHookImpl{
	protected static String getExtras(Intent intent) {
		//Log.i(_TAG, "--------- DENTRO GET EXTRAS -------------");
		String out = "";
		try {
			//Log.i(_TAG,"------------- Prendo gli extras");
			Bundle bundle = intent.getExtras();
		    if (bundle != null) {
		    	//Log.i(_TAG,"------------- PRESO gli extras");
				for (String key : bundle.keySet()) {
				    Object value = bundle.get(key);
				    out += String.format("--> [%s %s (%s)]\n", key,  
				        value.toString(), value.getClass().getName());
				}
				out = out.substring(0, out.length() - 1);
		    }
		    else{
		    	//Log.i(_TAG,"------------- NO extras");
		    }
		}
		catch (Exception e) {
			out = "Cannot get intent extra";
		}
		return out;
	}
	//tolto static
	public  void dump_intent(Object... args){
		//openfile();
		
		_logBasicInfo();
		//Log.i(_TAG, "-------------- SONO STATO CHIAMATO ------------");
		Intent intent = (Intent) args[1];
		//Log.i(_TAG, intent.toString());
		String out = "-> " + intent;	
		_logParameter("Intent", intent);
		String extra = getExtras(intent);
		if (!extra.isEmpty()) {
			_logParameter("Extra", extra);
			out += "\n-> Extra: \n" + extra + "";
		}
		if(debug)
			Log.i(_TAG,out);
		_logFlush_I(out);
	}
	public  void ipc_receiver(Object... args){
		//Log.i(_TAG, "-------------- ipc_receiver SONO STATO CHIAMATO ------------");
		String out = "";
		
		// arg1 is an intent filter
		IntentFilter intentFilter = (IntentFilter) args[2];
		if (intentFilter != null) {
			out = "-> Intent Filter: \n";
			for (int i = 0; i < intentFilter.countActions(); i++)
				out += "--> [Action "+ i +":"+intentFilter.getAction(i)+"]\n";
			out = out.substring(0, out.length() - 1);
		}
		
		// args[2] is the permissions
		if (args.length > 3 && args[3] != null) {
			out += ", permissions: " + args[2];
		}
		
		if (args.length == 3 || (args.length > 3 && args[3] == null))			
			if(debug)
				System.out.println("-> No permissions explicitely defined for the Receiver");
		else
			;
		//LoggingManager.sendLog(args);
	}
}
