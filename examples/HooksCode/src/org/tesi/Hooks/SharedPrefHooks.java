package org.tesi.Hooks;


import org.sid.addi.core.AppState;
import org.tesi.core.DalvikHookImpl;

import android.content.SharedPreferences;
import android.util.Log;

public class SharedPrefHooks extends DalvikHookImpl {
	
	//provo a togliere lo static
	public  void getSharedPref(Object... args){
		//System.out.println("GET SHARED PREF CHIAMATO");
		Object thiz = args[0];
		
		String prefName = (String) args[1]; // name of pref to retrieve
		if (prefName == null) {
			return;
		}
		/** FUNZIONAAAA
		SharedPreferences s = (SharedPreferences) thiz;
		String res = s.getString(prefName, "");
		Log.i(_TAG, "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA ritornato: "+res);
		*/
		// args[1] is the default value 


		String res = null;
		try {
			//o = _hookInvoke(args);
			SharedPreferences s = (SharedPreferences) thiz;
			res = s.getString((String)args[1],(String) args[2]);
			if(debug)
				System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA ritornato: "+res);
		} catch (Throwable e) {
			// this may throw if incorrect type specified in the code
			Log.w("IntrospyLog", "error in Intro_GET_SHARED_PREF: "+e);
		}
		
		
		_logParameter("Preference Name", args[1]);
		String out = "### PREF:"+AppState._packageName + 
						//":getSharedPref:"+ _methodName +
						"; name: [" + args[1] + "]" +
						", default: [" + args[2] + "]";


		if (res != null) {
			out += "; retrieves: ["+res+"]";
			if(debug)
				System.out.println("AAAA RET QUALCOSA: "+out);
			_logReturnValue("Value", res);
			_logFlush_I(out);
		}
		else {
			_logLine(out);
			_logFlush_I("-> Preference not found or incorrect type specified");
		}
		
		//System.out.println("GET SHARED PREF FINE");
		
		
		//System.out.println("ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZz");
		/*
		try {
			Class c = Class.forName("org.tesi.core.ObjWrapper");
			System.out.println("+++++++++++++++++++++Classe: "+c.toString());
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("TROVATA EXCEPTION ------------");
			e.printStackTrace();
		}
		*/
		//LoggingManager.sendLog(args);
	}
}
