package org.tesi.Hooks;

import java.io.File;

import org.sid.addi.core.AppState;
import org.tesi.core.DalvikHookImpl;

import android.util.Log;

public class FileSystemHooks extends DalvikHookImpl {

	protected static boolean is_SD_card(String path) {
		if (path != null && 
				(path.contains("sdcard") || 
				path.contains("/storage/"))) {
			// crashes with system app:
			// path.contains(Environment.getExternalStorageDirectory().toString()
			//super.execute(config, resources, old, args);
			return true;
		}
		return false;
	}
	//tolto static
	public  void check_dir(Object... args){
		//System.out.println("CHECK DIR CHIAMATO CON #ARGS: "+args.length);
		try {		
			String path = "[" +  args[1] + "]";
			_logParameter("Path", path);
			if (is_SD_card(path)) {
				if(debug)
					System.out.println("Read/write on sdcard: " + path);
				_logFlush_W("Read/write on sdcard: " + path);
			} else {
				// one liner on this to avoid too much noise
				_logFlush_I("### FS:"+ AppState.getPackageName() + ":" + path);
				if(debug)
					System.out.println("### FS:"+ AppContext.getPackageName() + ":" + path);
				//Log.i(_TAG,"### FS:"+ AppContextConfig.getPackageName() + ":" + path);
			}
	
		} catch (Exception e) {
			Log.w("IntrospyLog", "Exception in Intro_FILE_CHECK_DIR: " + e);
			//Log.w("IntrospyLog", "-> App path: " + AppContextConfig.getDataDir() + 
					//"\n" + e.fillInStackTrace());
		}
		//LoggingManager.sendLog(args);
	}
	//tolto static
	@SuppressWarnings("unused")
	public  void check_fs_perm(Object... args){
		//Log.i(_TAG, " ---------- CHECK FS PERM CHIAMATO!!! ----------");
		if ((Boolean)(args[1]) == true && (Boolean)args[2] == false) {
			File f = (File) args[0];
			if(debug)
				System.out.println("Writing file with WORLD read/write mode: " + 
						" in " + f.getAbsolutePath());
			_logBasicInfo();
			_logParameter("Mode", "WORLD read/write");
			_logParameter("Path", f.getAbsolutePath());
			_logFlush_W("Writing file with WORLD read/write mode: " + 
						" in " + f.getAbsolutePath());
		}		
	}
	//tolto static
	public  void check_fs_mode(Object... args){
		//Log.i(_TAG, " ---------- CHECK FS MODE CHIAMATO!!! ----------");
		//Log.i(_TAG, "Sono: " + get_className());
		if( args[0] == null  ||  args[1] == null){
			return;
		}
		// arg0 is the path
		String path = ": ["  + "/" +  (String)args[1] + "]";
		if (is_SD_card(path)) {
			if(debug)
				System.out.println("Read/write on sdcard: " + path);
			_logBasicInfo();
			_logParameter("Path", path);
			_logFlush_W("Read/write on sdcard: " + path);
		}
		else {
			// arg1 is the mode
			Integer mode = (Integer) args[2];
			
			String smode;
			switch (mode) {
				case android.content.Context.MODE_PRIVATE: 
					smode = "Private";
					break;
				case android.content.Context.MODE_WORLD_READABLE: 
					smode = "!!! World Readable !!!";
					break;
				case android.content.Context.MODE_WORLD_WRITEABLE: 
					smode = "!!! World Writable !!!";
					break;
				default: 
					smode = "???";
			}
			smode = "MODE: " + smode;
			
			if (mode == android.content.Context.MODE_WORLD_READABLE || 
					mode == android.content.Context.MODE_WORLD_WRITEABLE) {
				if(debug)
					System.out.println("Writing file with dangerous mode: " + 
							smode + " in " + path);
				_logBasicInfo();
				_logParameter("Mode", smode);
				_logFlush_W("Writing file with dangerous mode: " + 
							smode + " in " + path);

			}
		}
	}
}
