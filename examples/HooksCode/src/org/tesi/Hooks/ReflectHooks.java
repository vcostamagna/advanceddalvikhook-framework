package org.tesi.Hooks;

import org.tesi.core.DalvikHookImpl;

import android.util.Log;

public class ReflectHooks extends DalvikHookImpl {
	//removed static
		//public static Class<?> forName (String className)
		public  void forName(Object... args){
			if(debug)
				Log.i("Hooks", "+++++++++++++++++++++++++++++chiamato forName su: "+args[0]);
		}
		//public Method getMethod (String name, Class...<?> parameterTypes)
		public  void getMethod(Object... args){
			if(debug)
				Log.i("Hooks", "chiamato getMethod con: "+args[1]);
		}
}
