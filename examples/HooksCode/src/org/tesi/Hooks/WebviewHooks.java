package org.tesi.Hooks;

import org.tesi.core.DalvikHookImpl;

public class WebviewHooks extends DalvikHookImpl {
	
	public  void webview_js(Object... args){
		// this only display data when there is a potential issue
		_logBasicInfo();
		_logFlush_W("-> !!! Javascript interface " +
				"added for the webview. Details: " +
				args[1] + ", name: " + args[2]);	
	}
	//setPluginState(WebSettings.PluginState state) 
	// with android.webkit.WebSettings.PluginState.ON
	// setJavaScriptEnabled(boolean flag) with true
	// setAllowFileAccess (boolean allow)
	public  void webview_set(Object... args){
		if ((Boolean)args[1] == true) {
			_logBasicInfo();
			_logFlush_W("-> !!! Set of a potentially dangerous " +
					"functionality to true for the webview using "+
					get_method_name() + ", make sure this " +
					"functionality is necessary");
		}	

	}
}
