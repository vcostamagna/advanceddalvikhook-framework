package org.tesi.Hooks;

import java.security.Key;
import java.util.Stack;

import javax.crypto.Cipher;

import org.tesi.core.DalvikHookImpl;

import android.util.Log;


//cambiato static in tutti
public class CryptoHooks extends DalvikHookImpl{
	protected static Cipher 	_lastCipher;
	protected static Integer 	_lastMode;
	protected static 	Stack<byte[]> IVList = new Stack<byte[]>();
	protected static String 	_out = "";
	public static Cipher cipher = null;
	protected static boolean 	_warning = false;
	protected static void _getIV(Cipher cipher) {
		if (cipher.getIV() != null) {
			String iv = _getReadableByteArr(cipher.getIV());
			_out += "; IV: " + iv;
			//_logParameter("IV", iv);

			if (cipher.getIV()[0] == 0) {
				//Log.w("Introspy", "!!! IV of 0");
				_warning = true;
			}
			else {
				// check if this IV has already been used
				if (IVList.contains(cipher.getIV())) {
					_out  += " - !!! Static IV";
					_warning = true;
				}
				IVList.push(cipher.getIV());
				// keep a list of a max of 10 IVs
				if (IVList.size() > 10)
					IVList.pop();
			}
		}
	}	
	protected static void _getAlgo(Cipher cipher) {
		String algo = cipher.getAlgorithm();
		if (algo != null) {
			_out = "-> Algo: " + algo;
			//_logParameter("Algo", algo);
			if (cipher.getAlgorithm().contains("ECB")) {
				_warning = true;
				_out += " - !!! ECB used. ECB mode is broken and should not be used.";
			}
		}
	}
	private static void _getInput(byte[] data) {
		if (data != null && data.length != 0) { // when no args to doFinal (used only update())
			String i_sdata = null;
			i_sdata = new String(data);
			if (i_sdata != null && !i_sdata.isEmpty()) {
				if (_isItReadable(i_sdata)) {
					i_sdata = _byteArrayToReadableStr(data);
					//_logParameter("input (Encrypt)", i_sdata);
					//System.out.println("-> ENCRYPT: [" + i_sdata + "]");
				}
				else {
					String sdata = _byteArrayToB64(data);
					//System.out.println("-> Input data is not in a readable format, " +
					//			"base64: ["+ sdata +"]");
					//System.out.println("Output (converted to b64)"+ sdata);
				}
			}
		}
	}
	private static void _getOutput(Object... args) {
		byte[] data = null;
		String o_sdata = null;
//		if (cipher == _lastCipher && _lastMode == Cipher.DECRYPT_MODE)
			try {
				//data  = (byte[]) _hookInvoke(args);
				if(args.length > 0 &&  args[1] != null){
					//System.out.println(" DENTRO GET OUTPUT CHIAMO CON ARGOMENTI");
					byte[] in = (byte[]) args[1];
					//Object[] myargs = new Object[]{args};
					data = cipher.doFinal(in);
				}else{
					data = cipher.doFinal();
				}				
			}
			catch (Throwable e) {
				//System.out.println("doFinal function failed: "+e);
			}
			if (data != null) {
				o_sdata = new String(data);
				if (_isItReadable(o_sdata)) {
					o_sdata = _byteArrayToReadableStr(data);
					//System.out.println("-> DECRYPT: [" + o_sdata + "]");
				}
				else {
					String sdata = _byteArrayToB64(data);
					//System.out.println("-> Output data is not in a readable format," +
					//			" base64: ["+ sdata +"]");
				}
			}
//		} else {
//		}
	}
	public  void crypto_init(Object... args){
		System.out.println("DENTRO CRYPTO INIT ");
		// let's not care about init since we are hooking 
		// the key class already
		// BUT it can be useful to get a state of the mode 
		// if needed later
		if (args[0] != null) {
			//System.out.println("DENTRO CRYPTO INIT THIZ NON NULLO!!!");			
			try {
				_lastCipher = (Cipher) args[0];
				
				// get the mode
				Integer mode = _lastMode = (Integer) args[1];
				String smode;
				switch (mode) {
					case Cipher.DECRYPT_MODE: 
						smode = "DECRYPT_MODE";
						break;
					case Cipher.ENCRYPT_MODE: 
						smode = "ENCRYPT_MODE";
						break;
					default: 
						smode = "???";
				}
				
				
				String out = "-> Mode: " + smode;
				
				// get the key
				Key key = (Key) args[2];
				String skey = "";
				if (key != null) {
					skey = _getReadableByteArr(key.getEncoded());
					out += ", Key format: " + key.getFormat() + 
							", Key: [" + skey + "]";
				}

				System.out.println(out);
				
			} catch (Exception e) {
				System.out.println( "Error in Intro_CRYPTO: " + e);
			}
			
		}		
	}
	public  void keystore_hostname(Object... args){
		// arg2 is the passcode for this trustore
		if (args[3] != null) {
			String passcode = 
					_getReadableByteArr((byte[]) args[3]);
			System.out.println("-> TrustStore passcode: " + passcode);
		}		
	}
	public  void keystore(Object... args){
				// arg1 is the passcode for the trustore
		if (args[2] != null) {
			String passcode = 
					_getReadableByteArr((byte[]) args[2]);
			System.out.println("-> TrustStore passcode: " + passcode);
		}		
	}
	public  void get_key(Object... args){
		byte[] key = (byte[]) args[1];
		if (key != null) {
			String skey = _getReadableByteArr(key);

			System.out.println("-> Key: ["+skey+"], algo: "+args[2]);
		}
	}
	public  void crypto_pbekey(Object... args){

		String passcode = new String((char[])args[1]);
		String salt = null;
		int iterationCount = -1;
		if (args.length >= 2 && args[2] != null) {
			salt = 
				_byteArrayToReadableStr((byte[])args[2]);
			iterationCount = (Integer)args[3];

			// _logReturnValue("Key", _hookInvoke(args));
			System.out.println("-> Passcode: [" + passcode + "], Salt: [" + salt + 
					"], iterations: " + iterationCount + "");
		}
		else {
			System.out.println("Passcode: [" + passcode + "]");
		}
	}
	public  void doFinal2(Object... args){
		System.out.println("-------------- DEnTRO DOFINAL2-----------");
		if (args[0] != null) {
			_warning = false;
			_out = "";
			//Cipher cipher = (Cipher) _resources;
			System.out.println("--------------  DOFINAL2 prima di cipher-----------");
			cipher = (Cipher) args[0];
			System.out.println("-------------- DEnTRO dopo  cipher-----------");			
			if(cipher == null){
				System.out.println(" ERRRORE CIPHER ON CRYPTO EXECUTE");
				return;
			}
			else{
				System.out.println("  CIPHER ON CRYPTO EXECUTE");
			}
			//_logBasicInfo();

			// input
			if (args.length != 0 && args[0] != null) {
				try {
					_getInput((byte[]) args[0]);
				}
				catch (Exception e) {
					System.out.println("Error in _getInput " +
							"(CRYPTO_IMPL->dofinal): " + e);
				}
			}
			//output
			try {
				_getOutput(args);
			}
			catch (Exception e) {
				System.out.println("Error in _getOutput " +
						"(CRYPTO_IMPL->dofinal): " + e);
			}
			// algo/IV
			try {
				_getAlgo(cipher);
				_getIV(cipher);
			}
			catch (Exception e) {
				System.out.println( "Error in _getAlgo/_getCipher " +
						"(CRYPTO_IMPL->dofinal): " + e);
			}
			// dump some params
			if (cipher.getParameters() != null)// && ApplicationConfig.g_debug)
				System.out.println("Parameters: " + cipher.getParameters());

		
		}
		else {
			System.out.println("Error in Intro_CRYPTO: resource is null");
		}		
	}
}
