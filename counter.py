#!/usr/bin/python
import os

hooks = dict();


def parser(afile):
        global hooks
        with open(afile) as f:
            #read first line
            while 1 :
                num = f.readline()
                #print 'letto: '+num
                if not num: break
                target = f.readline()
                #print 'target: '+target
                if target in hooks.keys():
                    hooks[target] = hooks[target]+1
                else:
                    hooks[target] = 1
                for i in range(int(num)-1):
                    f.readline()
        f.close()

parser('stacktraces.log')
total = 0
for key in hooks:
    total += hooks[key]
    print key,hooks[key]

print 'totale metodi chiamati: '+ str(total)
