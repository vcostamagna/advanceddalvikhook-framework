counter=1
DIR=/mnt/sdcard/performancetests
if [ ! -d $DIR ]; then
  mkdir $DIR
  mkdir  $DIR/no-hooks
  mkdir $DIR/with-hooks
fi

function run(){
echo 'lancio test senza instrumentation...'
am instrument -w  insecureapp.tesi.org.insecureapp.test/android.test.InstrumentationTestRunner
#status=$?
#if [ $status == 0 ]; then
#	mv /mnt/sdcard/insecureapp.trace "/mnt/sdcard/performancetests/no-hooks/insecureapp$counter.trace"
#	((counter++))
#fi
}
function run_with_hooks(){
echo 'lancio test con instrumentation..'
sh runhijack.sh -s insecureapp.tesi.org.insecureapp
echo 'lancio test app'
am instrument -w  insecureapp.tesi.org.insecureapp.test/android.test.InstrumentationTestRunner
#status=$?
#if [ $status == 0 ]; then
#        mv /mnt/sdcard/insecureapp.trace "/mnt/sdcard/performancetests/with-hooks/insecureapp$counter.trace"
#        ((counter++))
#fi
}
for i in `seq 1 2`;
do
  logcat -c
  run_with_hooks
  logcat -d -s dalvikvm,DEBUG > "logoutput$i.log"
  PID=`ps | grep hijack  | awk '{print $2}'`
  if ! kill $PID > /dev/null 2>&1; then
    echo "Could not send SIGTERM to process $PID" >&2
  fi
done

#$counter=1
mv /mnt/sdcard/timing*.log $DIR/with-hooks/
mv /data/local/tmp/logcat*.log $DIR/with-hooks/

for i in `seq 1 2`;
do
  run
done
mv /mnt/sdcard/timing*.log $DIR/no-hooks/
#mv /data/local/tmp/logcat*.log $DIR/no-hooks/
