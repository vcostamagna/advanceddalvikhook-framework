import os,glob, math
import numpy as np
import matplotlib.pyplot as plt

width = 0.35       # the width of the bars
runType = ["no-hooks","with-hooks"]
typeofActivity = ["cryptoActivity","dynReceiverActivity","FSwriteactivity","hashActivity","reflectActivity"]
N=len(typeofActivity) #numero di activity
ind = np.arange(N)  # the x locations for the groups
fig = None
ax = None

class Plotter(object):
	def __init__(self,dirname):
		self.with_hooks_values = []
		self.no_hooks_values = []
		self.dir = dirname
		os.chdir(self.dir)
	def maker(self):
		global runType
		print os.getcwd()
		for d1 in runType:
			os.chdir(d1)
			#make work
			afiles = glob.glob('*.result')
			files = sorted(afiles,key=str.lower)
			for f in files:
				if f.endswith(".result"):
					self.collector(f,d1)
			os.chdir("../")
		print self.no_hooks_values
		print self.with_hooks_values
		self.normalize()
		self.pittore()
	def collector(self,fname,tipo):
		fp = open(fname,'r')
		for line in fp:
			res = math.ceil(float(line))
			if tipo == "no-hooks":
				self.no_hooks_values.append(res)
			else:
				self.with_hooks_values.append(res)
		fp.close()
	def normalize(self):
		for pos in range(len(self.with_hooks_values)):
			diff = abs(self.with_hooks_values[pos] - self.no_hooks_values[pos])
			if self.with_hooks_values[pos] < self.no_hooks_values[pos]:
				mmin = self.with_hooks_values[pos]
				if mmin < 20:
					mmin = mmin * 20
					other = mmin + diff
					self.with_hooks_values[pos] = mmin
					self.no_hooks_values[pos] = other
			else:
				mmin = self.no_hooks_values[pos]
				if mmin < 20:
					mmin = mmin * 20
					other = mmin + diff
					self.no_hooks_values[pos] = mmin
					self.with_hooks_values[pos] = other
	def autolabel(self,rects):
	    for rect in rects:
	    	height = rect.get_height()
	        ax.text(rect.get_x()+rect.get_width()/2., 1.05*height, '%d'%int(height),
	                ha='center', va='bottom')
	def pittore(self):
		global fig,ax
		fig, ax = plt.subplots()
		rects1 = ax.bar(ind, self.no_hooks_values, width, color='r')
		rects2 = ax.bar(ind+width, self.with_hooks_values, width, color='y')
		ax.set_ylabel('MilliSeconds')
		ax.set_title('Performances')
		ax.set_xticks(ind+width)
		ax.set_xticklabels( tuple([item.replace("Activity","") for item in typeofActivity]))
		ax.legend( (rects1[0], rects2[0]), ('no-hooks', 'with-hooks') )
		self.autolabel(rects1)
		self.autolabel(rects2)
		#plt.show()
		old = os.getcwd()
		astr = os.path.basename(self.dir)+'.png'
		os.chdir(self.dir)
		plt.savefig(os.path.basename(self.dir+'.png'),bbox_inches='tight')
		os.chdir(old)


class Test(object):
	def __init__(self, tag, dirName):
		self.dirName = dirName
		self.media = 0
		self.tag = tag
		self.values = []
		self.typeOfTest = ["withControlCode", "noControlCode"]
		#self.runNumber = ["primogiro","secondogiro","terzogiro"]
		self.runType = ["no-hooks","with-hooks"]
	def run(self):
		print 'lancio con tag: '+self.tag
		os.chdir(self.dirName)
		for d1 in self.typeOfTest:
			old_d1 = os.getcwd()
			os.chdir(d1)
			for run in self.runType:
				old_run = os.getcwd()
				os.chdir(run)
				#make work
				for f in os.listdir(os.getcwd()):
					if f.endswith(".log"):
						self.parser(f)
				self.mymedia()
				self.cippa()
				self.clean()
				os.chdir(old_run)
			os.chdir(old_d1)
	def cippa(self):
		fp = open(os.getcwd()+'/'+self.tag+'.result', 'w')
		fp.write(str(self.media))
		fp.close()
	def clean(self):
		self.media = 0
		self.values = []
	def mymedia(self):
		for v in self.values:
			self.media += float(v);
		self.media = self.media/len(self.values)
		#print self.media
	def printValues(self):
		for v in self.values:
			print v
	def parser(self,fname):
		fp = open(fname, 'r')
		for line in fp:
			if self.tag in line:
				tokens = line.split(':')
				#print tokens[1]
				self.values.append(tokens[1].rstrip())
		#self.printValues()
		fp.close()


for tipo in typeofActivity:
	t1 = Test(tipo,"/home/sid/Dropbox/MasterThesis/repos/advanceddalvikhook-framework/performances")
	t1.run()
plot = Plotter("/home/sid/Dropbox/MasterThesis/repos/advanceddalvikhook-framework/performances/withControlCode");
plot.maker()
plot = Plotter("/home/sid/Dropbox/MasterThesis/repos/advanceddalvikhook-framework/performances/noControlCode");
plot.maker()