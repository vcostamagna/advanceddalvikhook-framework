/*
 *  Collin's Dynamic Dalvik Instrumentation Toolkit for Android
 *  Collin Mulliner <collin[at]mulliner.org>
 *
 *  (c) 2012,2013
 * 
 *	Valerio's Advanced DDI
 *	(c) 2014
 *
 *  License: LGPL v2.1
 *
 */




#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdbool.h>
#include <sys/mman.h>
#include <poll.h>
#include <sys/prctl.h>

#include "dexstuff.h"
#include "dalvik_hook.h"
#include "lista.h"
#include "Misc.h"
#include "log.h"
#include "strmap.h" 

//lib per intercettare i crash nativi
#include "coffeecatch.h"


#define APICLASS "Lorg/sid"
#define HOOKCLASS "Lorg/tesi"
#define APICLASS2 "org.sid"
#define HOOKCLASS2 "org.tesi"

#define DALVIKHOOKCLS "org/sid/addi/core/DalvikHook" //used by env->findclass()
#define DALVIKHOOKCLS_2 "Lorg/sid/addi/core/DalvikHook;"

#define LOADER "/data/local/tmp/loader.dex"
#define APIFILE "/data/local/tmp/apiclasses.dex"


struct dexstuff_t d;
struct dalvik_hook_t *loadClassdh;
lista L; // lista di dalvik_hook_t
pthread_mutex_t mutex; //list mutex
pthread_rwlock_t _rwlock;
pthread_rwlockattr_t attr;
pthread_mutex_t cop; //list mutex
static int debug = 0;
static int cookie = 0; 
static int cookie2 = 0;
pthread_t pty_t;
pthread_t tmp_t;
char working_dir[256] = {0};
static JavaVM* g_JavaVM = NULL;
StrMap *sm;

/* reference a classi utilizzate dal codice */
struct ClassObject *dalvikHookCls=NULL;

/* references condivise dai threads */
jclass stackelem = NULL;
jclass th = NULL;

//thread pipe
int pipefd[2];

/**
	Inizializza la struttura dalvik_hook_t con i dati relativi al metodo da hookare
*/
//int dalvik_hook_setup(struct dalvik_hook_t *h, char *cls, char *meth, char *sig, int ns, void *func)
int dalvik_hook_setup(struct dalvik_hook_t *h, char *cls, char *meth, char *sig,  void *func)
{
	if (!h)
		return EXIT_FAILURE;
	strcpy(h->clname, cls);
	strncpy(h->clnamep, cls+1, strlen(cls)-2);
	h->clnamep[strlen(cls)-2] = '\0';
	strcpy(h->method_name, meth);
	strcpy(h->method_sig, sig);
	h->n_iss = 0; //ns
	h->n_rss = 0; //ns
	h->n_oss = 0;
	h->native_func = func;
	h->sm = 0; // set by hand if needed
	h->af = 0x0100; // native, modify by hand if needed
	h->resolvm = 0; // don't resolve method on-the-fly, change by hand if needed
	h->debug_me = 0;
	h->dump = 0;
	return EXIT_SUCCESS;
}

/**
	Imposta l'hook del metodo
*/
int dalvik_hook(struct dexstuff_t *dex, struct dalvik_hook_t *h)
{
	jthrowable exc;
	//cerco se la class è in memoria
	struct ClassObject* target_cls = dex->dvmFindLoadedClass_fnPtr(h->clname); //return classobject*
	// in caso negativo fail
	if (!target_cls) {
		if (h->debug_me)		
			log("XXX11 ERROR non ho trovato la classe: %s\n", h->clname)
		//target_cls = dex->dvmFindClass_fnPtr(h->clname, (struct Object*)classLoader);
		return EXIT_FAILURE;		
	}
	if(h->debug_me){
		log("DENTRO DALVIK HOOK MAIN, cerco classe = %s\n",h->clname)
		log("dalvik_hook: class %s\n", h->clname)
		log("class = 0x%p, classLoaader = %p\n", target_cls, target_cls->classLoader)
	}	
	// print class in logcat
	if (h->dump && dex && target_cls)
		dex->dvmDumpClass_fnPtr(target_cls, (void*)1);
    
	//cerco il metodo da hookare tra i  metodi non-diretti
	h->method = dex->dvmFindVirtualMethodHierByDescriptor_fnPtr(target_cls, h->method_name, h->method_sig);
    
	if (h->method == 0) {
		if(h->debug_me)
			log("calling dvmFindDirectMethodByDescriptor_fnPtr\n");
		//cerco il metodo target tra i metodi diretti, devo escludere quelli nativi!
		h->method = dex->dvmFindDirectMethodByDescriptor_fnPtr(target_cls, h->method_name, h->method_sig);
        if(h->method == 0){
            if(debug)
                log("errore dalvik hook non ho trovato metodo %s!!\n",h->method_name)
            return EXIT_FAILURE;
        }
	}
    //metodo nativo, no hook
    if(d.dvmIsNativeMethod_fnPtr(h->method) == 1){
        if(debug)
        	log("errore dalvik_hook metodo nativo, nativeFunc: %p!!! \n", h->method->nativeFunc)
        return EXIT_FAILURE;
    }
	// constructor workaround, see "dalvik_prepare" below
	if (!h->resolvm) {
		h->cls = target_cls;
		h->mid = (void*)h->method;
		if(debug)
			log("XXXD: h->method vale: 0x%p, 0x%p\n", h->method, h->mid)
	}	
	if (h->debug_me)
		log("%s(%s) = 0x%p\n", h->method_name, h->method_sig, h->method)
	if (h->method) {
		if (h->debug_me)
			log("DALVIK HOOK COPIO METHOD %p %s\n", h->method, h->method_name)
		//copio il metodo originale
		struct Method* cippa = (struct Method*) dex->dvmLinearAlloc_fnPtr(((struct ClassObject*)target_cls)->classLoader,sizeof(struct Method));//(struct Method*)calloc(1,sizeof(struct Method));
		memcpy( cippa, h->method, sizeof(struct Method));
		//aggiungo il metodo appena copiato nella struttura dalvil_hook_t
		h->originalMethod = cippa;
		//impostato a 1 se il metodo target e' statico
		h->sm = is_static(dex, h->method);
		//bytecode del metodo
		h->insns = h->method->insns;
		if (h->debug_me) {
			log("DALVIK_HOOK h->method->nativeFunc 0x%p, h->nativefunc: 0x%p\n", h->method->nativeFunc, h->native_func)		
			log("DALVIK_HOOK insSize = %d  registersSize = %d  outsSize = %d\n", h->method->insSize, h->method->registersSize, h->method->outsSize)
		}
		//imposto grandezza registri, argomenti in ingresso, outs
		h->iss = h->method->insSize;
		h->rss = h->method->registersSize;
		h->oss = h->method->outsSize;
		
		//uso una funzione dalvik per calcolare la grandezza  degli argomenti del metodo
		int res = dex->dvmComputeMethodArgsSize_fnPtr(h->method);

		if (h->debug_me) {
			log("shorty %s\n", h->method->shorty)
			log("name %s\n", h->method->name)
			log("arginfo %x\n", h->method->jniArgInfo)
			log("#args = %d\n", res)
		}

		h->method->a = h->method->a | h->af; // make method native
		//imposto la funzione nativa
		h->method->nativeFunc = h->native_func;
		h->method->insns = (const u2*)  cippa;
		h->method->registersSize = cippa->insSize;
		h->method->outsSize = 0;
		h->n_iss  = res;

		if (h->debug_me){
			log("METODO CON %d ARGOMENTI \n", res)
			log("DALVIK_HOOK nativeFunc %p, dalvin_hook_t: %p, method->insns = %p\n", h->method->nativeFunc, h->native_func, h->method->insns)		
			log("DALVIK_HOOK insSize = %d  registersSize = %d  outsSize = %d\n", h->method->insSize, h->method->registersSize, h->method->outsSize)
			log("DALVIK HOOK ORIGINAL nativeFunc %p,  method->insns = %p\n", h->originalMethod->nativeFunc, h->originalMethod->insns)		
			log("DALVIK HOOK ORIGINAL insSize = 0x%p  registersSize = 0x%p  outsSize = 0x%p\n", h->originalMethod->insSize, h->originalMethod->registersSize, h->originalMethod->outsSize)
			log("patched %s to: 0x%p\n", h->method_name, h->native_func)
			log("DALVIK_HOOK nativeFunc 0x%p, dalvin_hook_t: 0x%p  method->insns = %p\n", h->method->nativeFunc, h->native_func, h->method->insns)		
			log("DALVIK_HOOK insSize = 0x%p  registersSize = 0x%p  outsSize = 0x%p\n", h->method->insSize, h->method->registersSize, h->method->outsSize)
		}		
	}
	else {
		if (h->debug_me)
			log("could NOT patch %s\n", h->method_name)
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}

/*
 * Return a new Object[] array with the contents of "args".  We determine
 * the number and types of values in "args" based on the method signature.
 * Primitive types are boxed.
 *
 * Returns NULL if the method takes no arguments.
 *
 * The caller must call dvmReleaseTrackedAlloc() on the return value.
 *
 * On failure, returns with an appropriate exception raised.
 */
static struct ArrayObject* boxMethodArgs(struct Method* method, const u4* args)
{
    const char* desc = &method->shorty[1]; // [0] is the return type.
    int m_not_static = 0;
    size_t srcIndex = 0;
    size_t dstIndex = 0;
    /* count args */
    size_t argCount = d.dexProtoGetParameterCount_fnPtr(&method->prototype);
    if(!is_static(&d,method)){
        argCount++;
        m_not_static = 1;
    }
    log("BOXMETHODARGS: %d\n", argCount)
    ClassObject* coa = d.dvmFindSystemClass_fnPtr("[Ljava/lang/Object;");
    //(ClassObject*) ((struct DvmGlobals)d.gDvm).classJavaLangObjectArray;
    /* allocate storage */
    struct ArrayObject* argArray = d.dvmAllocArrayByClass_fnPtr( coa,
        argCount, ALLOC_DEFAULT);
    if (argArray == NULL)
        return NULL;
    struct Object** argObjects = (struct Object**)(void*)argArray->contents;
    if(m_not_static){
        log("BOX copio thiz: %p\n", args[0])
        //copy thiz
        argObjects[dstIndex++] =  (Object*) args[srcIndex++];
    }
    /*
     * Fill in the array.
     */
     //changed to make thiz space
    while (*desc != '\0') {
        char descChar = *(desc++);
        JValue value;

        switch (descChar) {
        case 'Z':
        case 'C':
        case 'F':
        case 'B':
        case 'S':
        case 'I':
            log("box I e compagnia\n")
            value.i = args[srcIndex++];
            argObjects[dstIndex] = (struct Object*) d.dvmBoxPrimitive_fnPtr(value,
                d.dvmFindPrimitiveClass_fnPtr(descChar));
            /* argObjects is tracked, don't need to hold this too */
            d.dvmReleaseTrackedAlloc_fnPtr(argObjects[dstIndex], NULL);
            dstIndex++;
            break;
        case 'D':
        case 'J':
            value.j = d.dvmGetArgLong_fnPtr(args, srcIndex);
            srcIndex += 2;
            argObjects[dstIndex] = (struct Object*) d.dvmBoxPrimitive_fnPtr(value,
                d.dvmFindPrimitiveClass_fnPtr(descChar));
            d.dvmReleaseTrackedAlloc_fnPtr(argObjects[dstIndex], NULL);
            dstIndex++;
            break;
        case '[':
        case 'L':
            log("box L\n")
            argObjects[dstIndex++] = (Object*) args[srcIndex++];
            break;
        }
    }

    return argArray;
}


//static void __attribute__ ((constructor)) dalvikhook_my_init(void);

static char logfile[256] = "/data/local/tmp/dynsec";
static char stacklogfile[256] = "/data/local/tmp/dynsec";

void logmsgtofile(char *msg)
{
	/*
    int fp = open(logfile, O_WRONLY|O_APPEND);
    write(fp, msg, strlen(msg));
    close(fp);
    */
    FILE* fp = fopen(logfile, "a+");
    fprintf(fp,"%s",msg);
    fclose(fp);
}
void logstacktofile(char *msg)
{
	/*
    int fp = open(logfile, O_WRONLY|O_APPEND);
    write(fp, msg, strlen(msg));
    close(fp);
    */
    FILE* fp = fopen(stacklogfile, "a+");
    fprintf(fp,"%s",msg);
    fclose(fp);
}
static void logmsgtostdout(char *msg)
{
	write(1, msg, strlen(msg));
}

void* dalvikhook_set_logfunction(void *func)
{
    void *old = libdalvikhook_log_function;
    libdalvikhook_log_function = func;
    //logmsgtofile("dalvik_set_logfunction called\n");
    return old;
}
void* dalvikhook_set_stacklogfunction(void *func)
{
    void *old = libdalvikhook_log_function;
    libdalvikhook_stacklog_function = func;
    //logmsgtofile("dalvik_set_logfunction called\n");
    return old;
}
static void dalvikhook_my_init(void)
{
    // set the log_function
	//logmsgtofile("dalvik init\n");
    libdalvikhook_log_function = logmsgtofile;
}

//void* onetoall2(u4* args, JValue* pResult, struct Method* method, struct Thread* self){
static void* myLoadClass(u4* args, JValue* pResult, struct Method* method, struct Thread* self){
	void *old = d.dvmChangeStatus_fnPtr(self, THREAD_NATIVE);
	//log("old status: %d\n", old)	struct dalvik_hook_t *res;

	//log("myloadclass chiamata\n")
	int srcIndex = 0;
	struct Method* original =(struct Method*) method->insns;
	void* thiz = (struct Object*)args[0];
	srcIndex++;
	void* string = args[1];
	char *c = d.dvmCreateCstrFromString_fnPtr((struct StringObject*)string);
	
	char *hash = dvmDotToSlash(c);
	//log("presi: thiz=%p, string=%p %s\n",thiz,string, c)
	//log("CERCO: %s\n", hash)
	//ottengo il metodo chiamato e creo l'hash da cercare nella lista

	char buf[255];
	int result = sm_get(sm, hash, buf, sizeof(buf));

	if(result){
		//log("trovato elemento in hashtable: %s\n",buf)
		//ora devo hookare tutti i metodi di questa classe presenti nell hashtable
	}
	//int callOriginalMethod(struct Method* original, struct Object* thiz, Thread* self, JValue* pResult, JValue* margs){
	int len = loadClassdh->n_iss;
	JValue* margs;
	margs = calloc(len, sizeof(JValue));
	const char* desc = &original->shorty[1]; //0 is return type
	int k =0 ;
	//loop sugli argomenti
	while(*desc != '\0'){
		char descChar = *(desc++);
		switch(descChar){
			case 'Z':
			case 'C':
			case 'F':
			case 'B':
			case 'S':
			case 'I':
				margs[k].i = args[srcIndex];
				if(debug)
					log("dentro I, %d, %d,srcIndex = %d args = %p \n", margs[k].i, args[srcIndex], srcIndex, args[srcIndex])
				k++; srcIndex++;
				break;
			case 'D':
			case 'J':
				if(debug)
					log("dentro J, k = %d, srcindex = %d, args = %p\n",k,srcIndex, args[srcIndex])
				memcpy(&margs[k++].j, &args[srcIndex], 8);
				srcIndex += 2;
				break;
			case '[':
			case 'L':
				if(debug)
					log("dentro L, k = %d, srcindex = %d, args = %p\n",k,srcIndex, args[srcIndex])
				margs[k].l = args[srcIndex];
				k++; srcIndex++;
				break;
			default:
				log("ONETOALL2 DESCCHAR NON RICONOSCIUTO %c\n", descChar)
				srcIndex++;
		}
	}
	if(callOriginalMethod(original, thiz, self,pResult,margs) == -1){
		if(debug)
			log("myloadclass trovata exception provo a vedere se la classe esiste\n")
		//dalvik_dump_class(&d,hash);
		//dalvik_dump_class(&d,"Lit/rcs/MainActivity;");
		//d.dvmClearException_fnPtr(self);
		if(d.dvmCheckException_fnPtr(self)){
			if(debug)
				log("myloadclass provadel9 ho ancora una exception.\n")
		}
		else{
			if(debug)
				log("myloadclass non ho trovato exception\n")
		}
		d.dvmChangeStatus_fnPtr(self, old);
		return -1;
	}
	d.dvmChangeStatus_fnPtr(self, old);
	return;
}
int placeSupportHook(){
	char *clname="Ljava/lang/ClassLoader;";
	char *mname= "loadClass";
	char *msig="(Ljava/lang/String;Z)Ljava/lang/Class;";
	loadClassdh =  (struct dalvik_hook_t *)malloc(sizeof(struct dalvik_hook_t));
	if(dalvik_hook_setup(loadClassdh, clname,mname,msig,&myLoadClass)){
		log("hook loadClass fallito!!\n")
		return -1; //fallito
	}
	dalvik_hook(&d, loadClassdh);
	log("placesupporthook terminato\n")
}
/**
	Funzione che esegue il codice di controllo associato all'hook

	TODO: 
	-segare via margs
	-gestire exception

	@env
	@original: puntatore all'oggetto che rappresenta il metodo originale (senza hook)
	@thiz: eventuale puntatore al this
	@margs
	@dh
	@self
	@oldargs
*/
struct Object* runJavaControlCode(JNIEnv*env,struct Method* original, struct Object* thiz, struct dalvik_hook_t* dh, Thread* self, u4* oldargs){
	//return;
	/*
	if(thiz!=NULL){
		oldargs++;
	}
	*/
	if(debug)
		log("DENTRO ESEGUIDEX2: name: %s\n", original->name)
	
	struct ClassObject *pd = _dvmFindLoadedClass(&d,"Lorg/tesi/core/ObjWrapper;");
	struct ClassObject *pd2 = _dvmFindLoadedClass(&d,"Lorg/tesi/core/DalvikHookImpl;");
	struct ClassObject *pd3 = _dvmFindLoadedClass(&d,"Lorg/tesi/core/LoggingManager;");
	if(pd && debug){
		log("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA trovata objwrapper\n")
		log("status wrapper: %d\n", pd->status)
		log("status dhook: %d\n", pd2->status)
		log("status logmanager: %d\n", pd2->status)
	}
	
	int real_args = dh->n_iss;//_dvmComputeMethodArgsSize(&d,res->method)
	JValue result;
	jclass clazz;;
	struct ClassObject* pCo;
	//costruisco oggetto [Ljava/lang/Object con gli argomenti oldargs
	struct ArrayObject* pAo = boxMethodArgs(original, oldargs);
	//prendo il riferimento all'oggetto DalvikHook contenuto nella struttura degli hook
	struct Object* oo = d.dvmDecodeIndirectRef_fnPtr(self, dh->ref);
	if(debug){
		log("ESEGUIDEX2 decodificata ref = %p, risultato = %p \n", dh->ref, oo)
		log("ESEGUIDEX2 ho un dex method da chiamare = %s!!!\n", dh->dex_meth)
	}
	if(debug){
		log("TESTING\n")
		struct ClassObject* xxx = dh->DexHookCls;
		log("classLoader dell'oggetto %p = %p \n", dh->DexHookCls, xxx->classLoader)
		log("classLoader dalvikhook %p = %p\n", oo, oo->clazz->classLoader)
	}
	//provo senza reflection
	
	char *dexclsname = dvmDotToSlash(dh->dex_class);
	//log("javacontrolcode dex class: %s, %s\n", dh->dex_class, dexclsname)
	struct ClassObject *dexCls = d.dvmFindLoadedClass_fnPtr(dexclsname);
	//free(dexclsname);
	if(!dexCls)
		log("javacontrolcode errore non trovo %s\n",dh->dex_class)
	if(dexCls->status < 7)
		_dvmInitClass(&d,dexCls);
	if(debug)
		log("javacontrolcode status class: %d\n",  dexCls->status)
	struct Method* dex_meth = d.dvmFindVirtualMethodHierByDescriptor_fnPtr(dexCls, dh->dex_meth, "([Ljava/lang/Object;)V");	
	if(!dex_meth)
		dex_meth = d.dvmFindDirectMethodHierByDescriptor_fnPtr(dexCls, dh->dex_meth, "([Ljava/lang/Object;)V");	
	//cerco l'entry point del codice DEX
	//struct Method* dex_meth = d.dvmFindVirtualMethodHierByDescriptor_fnPtr(dh->DexHookCls, "myexecute" , "([Ljava/lang/Object;)V");
	//struct Method* dex_meth = d.dvmFindVirtualMethodHierByDescriptor_fnPtr(dh->DexHookCls, "myexecute" , "()V");
	if(debug)
		log("ESEGUIDEX2 trovato %s = %p \n", dh->dex_meth, dex_meth)

	JValue* dexargs;
	dexargs = calloc(1, sizeof(JValue));
	//dexargs->l = mioArray;
	dexargs->l = pAo;
	JValue unused;
	//chiamo l'entry point del DEX, "myexecute"
	d.dvmCallMethodA_fnPtr(self,dex_meth,oo,false,&unused, dexargs );	

	if(unused.l != NULL){
		if(debug)
			log("XXX8 unusedvale: %p \n", unused.l)
		return unused.l;
	}
	else{
		if(debug)
			log("!!!!!!!!!!!!!!unused.l NULLLL\n")
	}
EXIT:
	if(debug)
		log("FINITO ESEGUIDEX DI: %s\n", dh->dex_meth);
	d.dvmReleaseTrackedAlloc_fnPtr(pAo, self);
	free(dexargs);

}

void ArrayObjectHelper(void* aobj){
	log("DENTRO ARRAY OBEJCT HELPER\n")
	struct ArrayObject* pAo = aobj;
	int i = 0;
	char* c = NULL;
	log("array lungo: %d\n", pAo->length)
	struct Object** argsArray = (struct Object**)(void*)pAo->contents;
	for(i=0; i<pAo->length; i++){
		log("pAo[%d]: %s\n",i, argsArray[i]->clazz->descriptor)
		c = d.dvmCreateCstrFromString_fnPtr((struct StringObject*)argsArray[i]);
		if(c != NULL){
			log("HELPER OTTENUTO: %s\n", c)
		}
	}
}

/**
	Funzione che esegue la chiamata al metodo originale

	TODO:
	-gestire exception


	@original: pointer al metodo originale
	@thiz
	@self
	@pResult
	@margs
*/
int callOriginalMethod(struct Method* original, struct Object* thiz, Thread* self, JValue* pResult, JValue* margs){
	JValue result;
	void *old = d.dvmChangeStatus_fnPtr(self, THREAD_RUNNING);
	if(debug)
		log("callOriginalV2 CHIAMO DVMCALLMETHODA = %s, self = %p,original = %p, result = %p, myargs = %p\n", original->name, self,original->insns,pResult,margs)

	d.dvmCallMethodA_fnPtr(self,original,thiz,false,&result, margs);
	if(d.dvmCheckException_fnPtr(self)){
		Object *excep = d.dvmGetException_fnPtr(self);
		if(debug)
			log("myloadclass trovata exception chiamando metodo: %s, tipo excep: %s!\n", original->name, 
			excep->clazz->descriptor)

		//d.dvmClearException_fnPtr(self);
		//d.dvmThrowChainedClassNotFoundException_fnPtr("porcodio",excep);
		//d.dvmThrowClassNotFoundException_fnPtr(excep->clazz->descriptor);
		if(debug)
			log("myloadclass trovata exception e fatto throw\n")
		return -1;
	}
	/**
	// exceptions are thrown to the caller with a return!
    if (d.dvmCheckException_fnPtr(self)) {
    	log("ERROR callOriginalV2 TROVATA EXCEPTION!!! \n")
    	Object* excep = d.dvmGetException_fnPtr(self);
    	if(excep != NULL){
            if(debug){
    		  log("XXX8 presa exception %p desc: %s\n", excep, excep->clazz->descriptor)
	    	  log("XXX111 lanciata except esco con -1\n")
            }
            
            d.dvmClearException_fnPtr(self);
            d.dvmThrowChainedClassNotFoundException_fnPtr(excep->clazz->descriptor,excep);
            return -1;
            //provo a capire come gestire le exception
            
            if( strcmp(excep->clazz->descriptor, "Ljava/lang/ClassNotFoundException;") == 0){
                log("XXX8 lancio class not found exception\n")
                d.dvmThrowClassNotFoundException_fnPtr("Class Not found!!");
            }else if (strcmp(excep->clazz->descriptor, "Ljava/io/FileNotFoundException;") == 0){
                d.dvmThrowFileNotFoundException_fnPtr("aaaa");
            }
            else if(strcmp(excep->clazz->descriptor, "Ljava/lang/NullPointerException;") == 0){
                d.dvmThrowNullPointerException_fnPtr("Class Not found!!");
            }
            else if(strcmp(excep->clazz->descriptor, "Ljava/lang/IllegalArgumentException;") == 0){
                log("XXX11 lancio illegal argument excep \n")
                d.dvmThrowIllegalArgumentException_fnPtr("Class Not found!!");
            }
            else{
                ;//d.dvmClearException_fnPtr(self);
            }
            d.dvmChangeStatus_fnPtr(self, old);
	        return -1;
	        
        }
    }  
    */
	struct ClassObject* returnType = d.dvmGetBoxedReturnType_fnPtr(original);
	if(debug)
		log("return from %s preso returnTYpe = %p, primitive = %d \n", original->name,returnType, returnType->primitiveType)
    if (returnType->primitiveType == PRIM_VOID) {
        // ignored
    } else if (result.l == NULL) {
        if (d.dvmIsPrimitiveClass_fnPtr(returnType)) {
        	if(debug)
            	log("null result when primitive expected!!!!!!!!!!!\n");
        }
        pResult->l = NULL;

    } else {
        //if (!d.dvmUnboxPrimitive_fnPtr(result.l, returnType, pResult)) {
        //    log("EXCEPTION!!! %p, %p", result.l->clazz, returnType);
        //}
        if(returnType->primitiveType == PRIM_NOT){
        	if(debug)
        		log("Tipo NON PRIMITIVO\n")
        	pResult->l = result.l;
        }
        else{
        	if(debug)
        		log("HO UN TIPO PRIMITIVO\n")
        	switch(returnType->primitiveType){
        		case PRIM_BOOLEAN:
        			pResult->z = result.z;
        			break;
        		case PRIM_INT:
        			pResult->i = result.i;
        			break;
        		case PRIM_CHAR:
        			pResult->c = result.c;
        			break;
        	}

        }        
    }
    d.dvmChangeStatus_fnPtr(self, old);
}


/*
	Funzione ausiliaria per copiare i dex tra le diverse istanze
*/
char* copyDexs(){
	log("dentro copy dex\n")
	FILE *exein, *exeout;

	exein = fopen(APIFILE, "rb");
	if (exein == NULL) {
	    /* handle error */
	    //perror("file open for reading");
		log("file open for reading\n");
		exit(EXIT_FAILURE);
	}
	char *fout = malloc(sizeof(char)*128);
	sprintf(fout, "%s/classes.dex", working_dir);
	exeout = fopen(fout, "wb");
	if (exeout == NULL) {
	    /* handle error */
	    //perror("file open for writing");
		log("file open for writing");
		exit(EXIT_FAILURE);
	}
	size_t n, m;
	unsigned char buff[8192];
	do {
		n = fread(buff, 1, sizeof buff, exein);
		if (n) m = fwrite(buff, 1, n, exeout);
		else   m = 0;
	} while ((n > 0) && (n == m));
	if (m) perror("copy");

	if (fclose(exeout)) perror("close output file");
	if (fclose(exein)) perror("close input file");
	return fout;
}

struct threadargs
{
  int fd;
};


/*
void mylog2(char * msg, int flag){
    FILE* afp = fopen(AFILE,"a+");
    fprintf(afp,"%s",msg);
    if(flag)
        fprintf(afp, "\n");
    fclose(afp);
}
*/
typedef struct my_message{
	struct Method** obj;
	int size;
	void* thread;
}my_message;
void consumer();

void lancia_consumer_producer(){
    // create inter-thread communication pipe
    //mylog2("LANCIATO CONSUMER PRODUCER\n");
    if (pipe(pipefd) != 0) {
        mylog2("pipe error\n");
        exit(1);
    }
    // create the producer thread
    pthread_t consumer_t;
    struct threadargs args;
    args.fd = pipefd[1]; // write end of the pipe
    pthread_create(&consumer_t, NULL, &consumer, NULL);
    // go into consumer event loop

    //consumer(pipefd[0]);

    // in the real world, we'd do pthread_join(), etc. here to clean up.
}
void producer(my_message *m){

    int rc = write(pipefd[1],&m,sizeof(struct my_message*));
    if(rc != sizeof(struct my_message* )){
        mylog2("producer error\n");
        exit(1);
    }
}
void saveStackTrace(struct Method** ppM, int len){
	struct Method* tmp;
	int i = 0;
	mylog2("%d\n", len);
	for(i=0;i<len;i++){
		tmp = ppM[i];
		//log("analyze, descriptor: %s, name: %s, shorty %s, native: %p\n",  tmp->clazz->descriptor,tmp->name, tmp->shorty, tmp->nativeFunc)
		mylog2("%s:%s:%s\n", tmp->clazz->descriptor, tmp->name, tmp->shorty);
	}
}

void consumer(){
  // set up file descriptors to do i/o on. In this example, just our
  // pipe from the consumer.
    prctl(PR_SET_NAME,"consumer-addi");
    int fd = pipefd[0];
	struct pollfd fds[1];
	fds[0].fd = fd;
	fds[0].events = POLLIN;

	for (;;) {

		// wait for an event from one of our producer(s)

		if (poll(fds, 1, -1) != 1) {
		  mylog2("poll error\n");
		  exit(1);
		}

		if (fds[0].revents != POLLIN) {
		  //mylog2("unexpected poll revents: %hd\n", fds[0].revents);
            mylog2("unexpected poll revents\n");
		  exit(1);
		}

		// read pointer to object from producer thread from pipe

		//struct Method** bar;
		my_message *m;
		if (read(fds[0].fd, &m, sizeof(m)) != sizeof(m)) {
		  mylog2("consumer read error\n");
		  exit(1);
		}

		//log("XXX11 obj ricevuto: 0x%p\n", m->obj)
		//struct Method**pp = m->obj;
		//struct Method*p = pp[0];
        //mylog2(bar->shorty,1);
        saveStackTrace(m->obj, m->size);
        //analyzeStackTrace((struct Method**) m->obj, m->size, 1);
		free(m);
		// the consumer owns the allocated object now, so it must delete it.		
	}  
}

int analyzeStackTrace(struct Method** pM, size_t len){
	struct Method* tmp;
	int i = 0;
	for(i=0;i<len;i++){
		tmp = pM[i];
		//
		if(debug){
			;//log("analyze, descriptor: %s, name: %s, shorty %s, native: %p\n",  tmp->clazz->descriptor,tmp->name, tmp->shorty, tmp->nativeFunc)
		}
		if(strstr(tmp->clazz->descriptor, HOOKCLASS) != NULL || strstr(tmp->clazz->descriptor, APICLASS) != NULL ){
			//log("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA CHIAMATO DA DIOOO\n")
			return 1;
		}
	}
	return 0;
}
/**
	Entry point di tutti i metodi hookati.
	http://androidxref.com/4.4.2_r2/xref/dalvik/vm/Jni.cpp#dvmCallJNIMethod
*/
void* onetoall2(u4* args, JValue* pResult, struct Method* method, struct Thread* self){
	//COFFEE_TRY() {
	//cambio lo status del Thread e salvo il vecchio
	void *old = d.dvmChangeStatus_fnPtr(self, THREAD_NATIVE);
	jthrowable exc;
	//recupero il metodo originale
	struct Method* original =(struct Method*) method->insns;
	//ottengo JNIEnv*
	JNIEnv *env = (JNIEnv*) get_jni_for_thread(&d);

	if(debug){
		log("DENTRO ONETOALL2\n")
		log("sono stato chiamato da original name %s, insns = %p, native = %p \n", original->name, original->insns, original->nativeFunc)
		log("sono stato chiamato da hooked name %s, insns = %p, native = %p\n", method->name, method->insns, method->nativeFunc);
		log("self vale: %p, env: %p\n", self,env)
	}
	struct dalvik_hook_t *res;
	char * cl = (char*)malloc(sizeof(char)*1024);
	char * name = (char*)malloc(sizeof(char)*1024);
	char * descriptor = (char*)malloc(sizeof(char)*1024);
	char * hash = (char*)malloc(sizeof(char)*2048);
	if(!cl || !name || !descriptor || !hash ){
		log("MALOCC ERORR!!!\n")
		return;
	}
	
	memset(cl,0,sizeof(cl));
	memset(name,0,sizeof(name));
	memset(descriptor,0,sizeof(descriptor));
	memset(hash,0,sizeof(hash));
	
	//ottengo il metodo chiamato e creo l'hash da cercare nella lista
	get_method(&d,name,descriptor);
	strcpy(hash,name);
	strcat(hash,descriptor);
    if(debug)
        log("CERCO NELLA LISTA : %s\n", hash)
	//certo nella lista codivisa l'hash del metodo hookato
	pthread_rwlock_rdlock(&_rwlock);
	res = (struct dalvik_hook_t *) cerca(L, hash);
	pthread_rwlock_unlock(&_rwlock);
	if(debug)
		log("preso res = %p, name = %s\n", res, res->clname)
	int len = res->n_iss;
	
	//descriptor del metodo senza il return type
	const char* desc = &original->shorty[1]; //0 is return type
	struct Object* thiz = NULL;
	size_t srcIndex = 0;
	size_t dstIndex = 0;

	//se il metodo non e' statico recupero il thiz
	if(!is_static(&d, original)){
		if(debug)
			log("ONETOALL2 PRENDO THIZ = %p \n", args[0])
		thiz = (struct Object*)args[0];
		srcIndex++;
	}

	JValue* margs;
	margs = calloc(len, sizeof(JValue));
	if(debug)
		log("ONETOALL2 metodo statico = %d \n", is_static(&d, original))

	int k =0 ;
	//loop sugli argomenti
	while(*desc != '\0'){
		char descChar = *(desc++);
		switch(descChar){
			case 'Z':
			case 'C':
			case 'F':
			case 'B':
			case 'S':
			case 'I':
				margs[k].i = args[srcIndex];
				if(debug)
					log("dentro I, %d, %d,srcIndex = %d args = %p \n", margs[k].i, args[srcIndex], srcIndex, args[srcIndex])
				k++; srcIndex++;
				break;
			case 'D':
			case 'J':
				if(debug)
					log("dentro J, k = %d, srcindex = %d, args = %p\n",k,srcIndex, args[srcIndex])
				memcpy(&margs[k++].j, &args[srcIndex], 8);
				srcIndex += 2;
				break;
			case '[':
			case 'L':
				if(debug)
					log("dentro L, k = %d, srcindex = %d, args = %p\n",k,srcIndex, args[srcIndex])
				margs[k].l = args[srcIndex];
				k++; srcIndex++;
				break;
			default:
				log("ONETOALL2 DESCCHAR NON RICONOSCIUTO %c\n", descChar)
				srcIndex++;
		}
	}
	int num = _computeStackFrameDepth(&d);
	struct Method** provami = malloc(sizeof(struct Method*)*num);
	_fillStrackTraceArray(&d,provami,num);
	int flag = analyzeStackTrace(provami, num);

	if(flag){
		if(debug)
			log("XXX7 SONO STATO CHIAMATO DAL FRAMEWORK\n")
		callOriginalMethod(original, thiz, self, pResult, margs);
		goto SAFE;
	}
	my_message *m = malloc(sizeof(struct my_message));
	m->obj = provami;
	m->size = num;
    producer(m);

	struct Object* tmp = NULL;
	//switch sui diversi tipi di Hook
	switch(res->type){
		case NORMAL_HOOK: 
			//prima eseguo il codice java
			if(strlen(res->dex_meth) > 0){
				d.dvmChangeStatus_fnPtr(self, THREAD_RUNNING);
				runJavaControlCode(env, original, thiz,  res, self, args);
				d.dvmChangeStatus_fnPtr(self, THREAD_NATIVE);
			}
			break;
		case TRUE_HOOK:
			pResult->z = true;
			goto SAFE;
		case FALSE_HOOK:
			pResult->z = false;
			goto SAFE;
		case NOX_HOOK:
			if(strlen(res->dex_meth) > 0){
				d.dvmChangeStatus_fnPtr(self, THREAD_RUNNING);
				tmp = runJavaControlCode(env, original, thiz, res, self, args);
				d.dvmChangeStatus_fnPtr(self, THREAD_NATIVE);
			}
			if(tmp != NULL){
				if(debug)
					log("XXX8 valore di ritorno DEX non NULLO: %p\n", tmp)
				pResult->l = tmp;
				goto SAFE;
			}
			else{
				log("XXX8 valore di ritorno DEX NULLO\n")
				goto SAFE;
			}
			break;
		case AFTER_HOOK:
			if(debug)
	  			log("AFTER HOOK, original meth = %p, hooked meth = %p \n", original->insns, method->insns)
			if(callOriginalMethod(original, thiz, self, pResult, margs) == -1){
				if(debug)
					log("ORIGINAL CALL FALLITA\n")
				goto SAFE;
			}
			d.dvmChangeStatus_fnPtr(self, THREAD_RUNNING);
			runJavaControlCode(env, original, thiz, res, self, args);
			d.dvmChangeStatus_fnPtr(self, THREAD_NATIVE);

			goto SAFE;	
		case UNUSED_HOOK:
			//hook disabilitato
			break;
		default:
			log("XXX9 ERRORE\n")
			//non dovremmo mai arrivare qui...
			break;
	}
	//chiamata del metodo originale
	callOriginalMethod(original, thiz, self, pResult, margs);

/*	
	
	} COFFEE_CATCH() {
		const char* message = coffeecatch_get_message();
		log("**FATAL ERROR: %s\n", message)
	}COFFEE_END();
	*/
SAFE:
	free(cl);
	free(name);
	free(descriptor);
	free(hash);
	free(margs);
	//ripristino il vecchio status del thread
	d.dvmChangeStatus_fnPtr(self, old);
	return;
}

/* 
	Metodi ausiliari per le chiamate ai metodi Java
*/

void* callStringObjectMethod(Thread* self, Method* m, Object* oo){
	char *c = NULL;
	JValue result;
	void* sobj = d.dvmCallMethodA_fnPtr(self, m, oo, true, &result, NULL );
	if(result.l){
		c = d.dvmCreateCstrFromString_fnPtr( (struct StringObject*) result.l);
	}
	if(debug)
		log("callstringobject: %s\n",c)
	return c;
}
int callIntMethod(Thread* self, Method* m, Object* oo){
	JValue result;
	void* sobj = d.dvmCallMethodA_fnPtr(self, m, oo, false, &result, NULL );
	return result.i;
}

static void iter(const char *key, const char *value, const void *obj)
{
   log("HASHTABLE key: %s value: %s\n", key, value)
}

/**
	Funzione richiamata da Java e utilizzata per impostare gli hook
*/
static int createHookFromJava( JNIEnv* env, jobject thiz, jobject clazz )
{
	//pthread_mutex_lock(&mutex);
	struct Thread* self = getSelf(&d);
	struct Object* oo = d.dvmDecodeIndirectRef_fnPtr(self, clazz);
	struct dalvik_hook_t *dh;
	//char **methods = (char *[]){"get_clname:()Ljava/lang/String;", "get_method_name:()Ljava/lang/String;","get_method_sig:()Ljava/lang/String;"};
	char methods[][512] = {"get_clname:()Ljava/lang/String;", "get_method_name:()Ljava/lang/String;","get_method_sig:()Ljava/lang/String;",
								"get_dex_method:()Ljava/lang/String;", "get_dex_class:()Ljava/lang/String;","get_hashvalue:()Ljava/lang/String;",
								"getType:()I", "isSkip:()I"};
	size_t ii = 0;
	char **tokens;
	char *token;

	char values[8][512] = {};

	char *dex_hash;
	char *dex_mname;
	char* dex_cls;
	char *dex_clname;
	char *dex_msig;
	char *dex_meth;
	int dex_type;
	int dex_isSkip;

	Method* clname ;
	Method* mname ;
	Method* msig ;
	Method* dexcls;
	Method* dexm ;
	Method* dexhash ;
	Method* dext;
	Method* dexskip;

	if(debug)
		log("------------ createHookFromJava!!! --------  clazz = 0x%p, thiz = 0x%p\n", clazz, thiz)
	if(!dalvikHookCls)
		dalvikHookCls = (struct ClassObject*) _dvmFindLoadedClass(&d, DALVIKHOOKCLS_2);
	if(dalvikHookCls){
		//log("createHookFromJava trovata dalvikhookclass!!\n")		  
		clname = d.dvmFindVirtualMethodHierByDescriptor_fnPtr(dalvikHookCls, "get_clname","()Ljava/lang/String;");
		mname = d.dvmFindVirtualMethodHierByDescriptor_fnPtr(dalvikHookCls, "get_method_name","()Ljava/lang/String;");
		msig = d.dvmFindVirtualMethodHierByDescriptor_fnPtr(dalvikHookCls, "get_method_sig","()Ljava/lang/String;");
		dexcls = d.dvmFindVirtualMethodHierByDescriptor_fnPtr(dalvikHookCls, "get_dex_class","()Ljava/lang/String;");
		dexm = d.dvmFindVirtualMethodHierByDescriptor_fnPtr(dalvikHookCls, "get_dex_method","()Ljava/lang/String;");
		dexhash = d.dvmFindVirtualMethodHierByDescriptor_fnPtr(dalvikHookCls, "get_hashvalue","()Ljava/lang/String;");
		dext = d.dvmFindVirtualMethodHierByDescriptor_fnPtr(dalvikHookCls, "getType","()I");
		dexskip = d.dvmFindVirtualMethodHierByDescriptor_fnPtr(dalvikHookCls, "isSkip","()I");
		if(clname){
			dex_clname = callStringObjectMethod(self, clname, oo);
			dex_mname = callStringObjectMethod(self, mname, oo);
			dex_msig = callStringObjectMethod(self, msig, oo);
			dex_cls = callStringObjectMethod(self, dexcls, oo);
			dex_meth = callStringObjectMethod(self, dexm, oo);
			dex_hash = callStringObjectMethod(self, dexhash, oo);
			dex_type = callIntMethod(self, dext, oo);
			dex_isSkip = callIntMethod(self, dexskip, oo);
		}
	}
	else{
		log("ERRORISSIMO non trovo dalvikHookCls %p \n", dalvikHookCls)
		return EXIT_FAILURE;
	}
	dh = (struct dalvik_hook_t *)malloc(sizeof(struct dalvik_hook_t));
	if(!dh){
		log("ERROR malloc dalvik_hook_t!!\n");
		return EXIT_FAILURE;
	}
	dh->type = dex_type;
	dh->loaded = 0;
	dh->ref = (*env)->NewGlobalRef(env, clazz);
	dh->DexHookCls = dalvikHookCls;
	strcpy(dh->dex_meth,"");
	if(strlen(dex_meth) > 0){
		strcpy(dh->dex_meth,dex_meth);
		strcpy(dh->dex_class,dex_cls);	
	}
	char *pointer;
	pointer = parse_signature(dex_msig);	
	int  check;
	if(*pointer == 'F'){
		//pthread_mutex_unlock(&mutex);
		return  EXIT_FAILURE;
		//hook a method that return jfloat
	}
	else if(*pointer == 'J'){
		//pthread_mutex_unlock(&mutex);
		return EXIT_FAILURE;

	}else if(*pointer == 'V'){
		if(dalvik_hook_setup(dh, dex_clname,dex_mname,dex_msig,&onetoall2))
			if(debug)
				log("----------- HOOK SETUP FALLITO ------------- : %s, %s\n", clname,mname)
		check =  dalvik_hook(&d, dh);
		//log("XXX7  check vale = 0x%p, %d\n",check, check)
		if(check == 0x1){
			if(debug)
				log("----------- HOOK VOID FALLITO ------------- : %s, %s\n", dex_clname,dex_mname)
			//sm_put(sm, dex_clname, "FALLITO");
			free(dh);
			//pthread_mutex_unlock(&mutex);
			return EXIT_FAILURE;
		}
		else
			if(debug)
				log("HOOK VOID PLACED, nome = %s\n", dh->originalMethod->name)			
	}
	else{
		if(dalvik_hook_setup(dh, dex_clname,dex_mname,dex_msig,&onetoall2))
			if(debug)
				log("----------- HOOK SETUP FALLITO ------------- : %s, %s\n", clname,mname)
		check =  dalvik_hook(&d, dh);
		//log("XXX7  check vale = 0x%p, %d\n",check, check)
		if(check == 0x1){
			if(debug)
				log("----------- HOOK FALLITO ------------- : %s, %s\n", dex_clname,dex_mname)
			//sm_put(sm, dex_clname, "FALLITO");
			//sm_enum(sm, iter, NULL);
			free(dh);
			//pthread_mutex_unlock(&mutex);
			return EXIT_FAILURE;
		}
		else
			if(debug)
				log("HOOK PLACED\n")	
		//log("XXX5 class loader = 0x%p\n", dh->method->clazz->classLoader)
	}
	if(debug)
		log("XXXD inserisco dh = 0x%p 0x%p, %s,%s,%s, hash = %s\n", &dh, dh, dex_clname,dex_mname,dex_msig,dex_hash)
	pthread_rwlock_wrlock(&_rwlock);
	int i = inserisci(&L,dh,dex_clname,dex_mname,dex_msig,dex_hash);
	pthread_rwlock_unlock(&_rwlock);
	if(debug)
		log("stampo lista: %d, dex_method = %s\n", i, dh->dex_meth)
	//pthread_mutex_unlock(&mutex);
	return EXIT_SUCCESS;
}

/**
	Funzione che crea il thread per mettersi in asccolta sulla PIPE e ricevere i comandi
*/

void* ptyServer(){
	//cazzi amari
	if(debug)
		log("XXX5 DENTRO THREADID = %d  PID = %d\n", pthread_self(), getpid())
    prctl(PR_SET_NAME,"pipeserver-addi");
	JNIEnv *envLocal;
    int status = (*g_JavaVM)->GetEnv(g_JavaVM,(void **) &envLocal, JNI_VERSION_1_6);
    if (status == JNI_EDETACHED)
    {
    	log("XXX5 chiamo attach\n")
        status = (*g_JavaVM)->AttachCurrentThread(g_JavaVM,&envLocal, NULL);
        if (status != JNI_OK)
        	if(debug)
            	log("XXX5 AttachCurrentThread failed %d",status)
        else	
        	if(debug)
        		log("XXX5 attach success = %d\n", status)
    }

	//XXX
	//jclass mycls2 = (*envLocal)->FindClass(envLocal, "org/sid/addi/core/Session");
	//jclass mycls2 = (*envLocal)->FindClass(envLocal, "org/sid/addi/core/PipeServer");
	//log("XXX5 trovata classe controller = 0x%p\n", mycls2)
	//jmethodID constructor2 = (*envLocal)->GetMethodID(envLocal, mycls2, "<init>", "(Ljava/lang/String;Ljava/lang/String;)V");
	
	
	//void *mycls = _dvmFindLoadedClass(&d, "Lorg/sid/addi/core/Session;");

	struct ClassObject *mycls = _dvmFindLoadedClass(&d, "Lorg/sid/addi/core/PipeServer;");
	while(mycls->status != 7){
		if(debug)
			log("Dentro while status not 7!! 0x%p  \n", mycls->status)
		sleep(2);
		if(debug)
			log("PTYSERVER status class: %d\n", mycls->status);
		_dvmInitClass(&d,mycls);
	}
	if(debug)
		log("cerco costruttore e start\n")
	void *constructor = d.dvmFindDirectMethodHierByDescriptor_fnPtr(mycls, "<init>" , "(Ljava/lang/String;Ljava/lang/String;)V");
	void *start = d.dvmFindVirtualMethodHierByDescriptor_fnPtr(mycls, "start" , "()V");
	if(debug)
		log("XXX9 trovato pipeserver: %p, <init>: %p, start: %p\n", mycls, constructor,start)
	
	//strcpy(res,"/data/local/tmp/res.fifo");
	//strcpy(req,"/data/local/tmp/req.fifo");
	char *res = malloc(sizeof(char)*256);
	char *req = malloc(sizeof(char)*256);
	sprintf(res,"%s/res.fifo",working_dir);
	sprintf(req,"%s/req.fifo", working_dir);
	if (mkfifo(res, 0777) == -1) { 
		if (errno!=EEXIST) {
			log("XXX5 pty error %s\n", strerror(errno))
			return;
		}
	}
	if (mkfifo(req, 0777) == -1) { 
		if (errno!=EEXIST) {
			log("XXX5 pty error %s\n", strerror(errno))
			return;
		}
	}
	
	//struct StringObject* jres = d.dvmCreateStringFromCstr_fnPtr(res);
	void* jres  = d.dvmStringFromCStr_fnPtr(res, strlen(res), ALLOC_DEFAULT);
	d.dvmReleaseTrackedAlloc_fnPtr(jres,getSelf(&d));
	//jobject gstr = (*envLocal)->NewGlobalRef(envLocal, jres);	
	void* jreq = d.dvmStringFromCStr_fnPtr(req, strlen(req), ALLOC_DEFAULT);
	d.dvmReleaseTrackedAlloc_fnPtr(jreq,getSelf(&d));
	//jobject gstr1 = (*envLocal)->NewGlobalRef(envLocal, jreq);
	
	//struct StringObject* jreq = d.dvmCreateStringFromCstr_fnPtr(req);
	if(debug)
		log("XXX9 creati string obj, jres: %p, jreq: %p\n", jres, jreq)
	jvalue args[2];
	args[0].l = jres;
	args[1].l = jreq;
	JValue result;

	void *instance = d.dvmAllocObject_fnPtr(mycls,ALLOC_DEFAULT);
	d.dvmReleaseTrackedAlloc_fnPtr(instance,getSelf(&d));
	d.dvmCallMethodA_fnPtr(getSelf(&d),constructor, instance, false, &result, args);
	d.dvmCallMethodA_fnPtr(getSelf(&d),start, instance, false, &result, NULL);
	
	/*
	jobject str = (*envLocal)->NewStringUTF(envLocal, req);
	jobject gstr = (*envLocal)->NewGlobalRef(envLocal, str);
	jobject str1 = (*envLocal)->NewStringUTF(envLocal, res);
	jobject gstr1 = (*envLocal)->NewGlobalRef(envLocal, str1);
	log("XXX5 CHIAMO NEW OBJECT\n")
	jobject mobj = (*envLocal)->NewObject(envLocal, mycls, constructor, gstr, gstr1); 
	jmethodID start = (*envLocal)->GetMethodID(envLocal, mycls, "start", "()V");
	log("XXX5 chiamo metodo su obj = 0x%p, start = 0x%p\n", mobj, start)
	(*envLocal)->CallVoidMethod(envLocal, mobj, start, NULL);
	*/




	(*g_JavaVM)->DetachCurrentThread(g_JavaVM);
	free(res);
	free(req);
	if(debug)
		log("XXX5 ESCO DA THREAD\n")
	pthread_exit(1);
}

void* _createPTY(){
	log("XXX4 CREATE PTY\n")
    int rc = pthread_create(&pty_t, NULL, ptyServer, NULL);
    if(rc){
    	log("XXX3 ERROR PTHREAD CREATE: %d\n", rc)
    }
    else{
    	log("XXX4 CREATO THREAD %d\n", rc)
    }
}


void* _th_wrapper(){
	log("XXX7 DENTRO THREAD WRAPPER CIPOLLA\n")
	JNIEnv *envLocal;
    int status = (*g_JavaVM)->GetEnv(g_JavaVM,(void **) &envLocal, JNI_VERSION_1_6);
    if (status == JNI_EDETACHED)
    {
    	log("XXX5 chiamo attach\n")
        status = (*g_JavaVM)->AttachCurrentThread(g_JavaVM,&envLocal, NULL);
        if (status != JNI_OK)
            log("XXX5 AttachCurrentThread failed %d",status)
        else	
        	log("XXX5 attach success = %d\n", status)
    }
	log("XXX5 preso env  = 0x%p, myenv = 0x%p, jvm = 0x%p\n", envLocal, envLocal, g_JavaVM)
	//_suspendAllT(&d);
	//handleAllMethodClass(&d,envLocal);
	//_resumeAllT(&d);
	(*g_JavaVM)->DetachCurrentThread(g_JavaVM);
	log("XXX5 ESCO DA THREAD\n")
	pthread_exit(1);
}

void* _newThread(){
	log("XXX7 dentro NEW THREAD\n")
	int rc = pthread_create(&tmp_t, NULL,_th_wrapper, NULL);
}

void *_loadSystemClass(JNIEnv *env, jobject str){
	void* vmloader = (*env)->FindClass(env, "java/lang/VMClassLoader");
	char *clsname = (*env)->GetStringUTFChars(env, str, NULL);
	jmethodID loadClassid = (*env)->GetStaticMethodID(env, vmloader,"loadClass", "(Ljava/lang/String;Z)Ljava/lang/Class;");
	if(loadClassid){
		jclass res2 = (*env)->CallStaticObjectMethod(env, vmloader, loadClassid, str, 0x0);
		if(res2){
			return res2;
		}
		else
			return NULL;
	}
}


/**
	Funzione di prova per manipolare il bytecode a runtime
*/
void* startMagic(JNIEnv *env){
	/**
	struct DvmDex* pdex;
	int res = _dvmDexFileOpenFromFd(&d,cookie, pdex);
	log("CI SONO1 %d\n",res)
	*/
	//devo aprire il DEX
	struct DexOrJar* dexfd = dexstuff_loaddex(&d, "/mnt/sdcard/whatsapp.dex");
	if(dexfd){
		log("XXX7 PROVA PROVA dex at = 0x%p, 0x%p, 0x%p, %s\n", dexfd->pDexMemory, dexfd->isDex,dexfd->pRawDexFile,dexfd->fileName)
		struct RawDexFile* p = dexfd->pRawDexFile;
		log("CI SONO1\n")
		if(p){
			log("CI SONO2\n")
			if(p->pDvmDex){
				log("CI SONO3\n")
				struct DvmDex* p2 = p->pDvmDex;
				log("CI SONO4\n")
				if(p2){
					log("CI SONO5\n")
					log("XXX7 p2 = %p, pp = %p, memmap = 0x%p, dexfile = 0x%p\n", p2, p->pDvmDex,  p2->memMap, p2->pDexFile)
					struct MemMapping mm = p2->memMap;
					log("CI SONO6\n")
					log("memmap add = %p,%p length = %d\n", mm.addr, p2->memMap, mm.length)
					u1 *a = mm.addr;
					log("value %p = 0x%02x\n",a+0x28, *a)
					//int res = _sysChangeMapAccess(0x02, 1, 1, NULL);
					u1 new = 0x66;
					//_dvmDexChangeDex1(p2, (u1*) a+0x28, new);
					log("value %p = 0x%02x\n",a, *a)
					log(" dump memmapping ")
					log("  addr = %p , length = %zu, base = %p, baselength = %zu\n", mm.addr, mm.length, mm.baseAddr, mm.baseLength)
					//log("ACCESS CHANGED!!\n")
					struct DexFile* p3 = p2->pDexFile;
					mprotect((void*)mm.addr, mm.addr + mm.length, PROT_READ|PROT_WRITE|PROT_EXEC);
					if(p3){
						struct DexHeader*  p4 = p3->pHeader;
						log("dexfile length = %d \n", p4->fileSize)
					}
				}
			}
		}
		log("CI SONO END\n")
		//log("memmap = 0x%p, dexfile = 0x%p, base =0x%p\n",dexfd->pRawDexFile->pDvmDex->memMap, dexfd->pRawDexFile->pDvmDex->pDexFile,
		//			dexfd->pRawDexFile->pDvmDex->pDexFile->baseAddr)	
	}
	
}
	
/**
	Funzione per aggiungere hook a runtime

	TODO:
	aggiungere HookType tra gli argomenti
*/
int createHooKFromNative(JNIEnv *env, char* clsname, char* methodname, char* methodsig, void* classLoader){
	//log("DENTRO CREATE AND INSERT DHOOK, clanme = %s, mname = %s, msid = %s\n", clsname, methodname, methodsig)
	//pthread_mutex_lock(&cop);
	char * cl = calloc(1024, sizeof(char));
	char * name = calloc(1024, sizeof(char));
	char * descriptor = calloc(1024,sizeof(char));
	if(cl == NULL || name == NULL || descriptor == NULL){
        log("MEMORY ERROR\n")
        return EXIT_FAILURE;
    }

	memcpy(cl, clsname, sizeof(clsname)*strlen(clsname));
	memcpy(name, methodname, sizeof(methodname)*strlen(methodname));
	memcpy(descriptor, methodsig, sizeof(methodsig)*strlen(methodsig));
	
	log("createAndInsertDhook, copiato cl = %s, name = %s, sign = %s , classloader= %p\n ", cl, name, descriptor, classLoader)

	struct dalvik_hook_t *dh;
	jobject clazz = NULL;
	dh = malloc(sizeof(struct dalvik_hook_t));
    if(dh == NULL){
        log("ERROR MEMORY \n")
        return EXIT_FAILURE;
    }
	//dh->real_args = args;
	dh->loaded = 0;
	dh->ok =1;
	dh->ref = NULL;//(*env)->NewGlobalRef(env, clazz);
	dh->type = NORMAL_HOOK;
	strcpy(dh->dex_meth,"");
	char *pointer;
	pointer = parse_signature(methodsig);	
	int  check;
	if(*pointer == 'F'){
		free(dh);
        free(cl);
        free(name);
        free(descriptor);
        //pthread_mutex_unlock(&cop);
		return EXIT_FAILURE;
		//hook a method that return jfloat
	}
	else if(*pointer == 'J'){
		free(dh);
        free(cl);
        free(name);
        free(descriptor);
		//pthread_mutex_unlock(&cop);
		return EXIT_FAILURE;
	}
	else if(*pointer == 'I'){
		free(dh);
        free(cl);
        free(name);
        free(descriptor);
		//pthread_mutex_unlock(&cop);
		return 0;
	}
	else{
		if(dalvik_hook_setup(dh, cl, name,descriptor,&onetoall2))
			if(debug)
				log("----------- createAndInsertDhook HOOK SETUP FALLITO ------------- : %s, %s\n", clsname, methodname)
		check =  dalvik_hook(&d, dh);
		log("XXX7  check vale = 0x%p, %d\n",check, check)
		if(check == 0x1){
			if(debug)
				log("----------- createAndInsertDhook HOOK FALLITO ------------- : %s, %s\n",clsname, methodname)
			free(dh);
            free(cl);
            free(name);
            free(descriptor);
			//pthread_mutex_unlock(&cop);
			return EXIT_FAILURE;
		}
		else
			if(debug)
				log("createAndInsertDhook HOOK PLACED\n")	
		//log("XXX5 class loader = 0x%p\n", dh->method->clazz->classLoader)
	}

	log("DENTRO CREATE AND INSERT DHOOK22222, clanme = %s, mname = %s, msid = %s\n", cl, name, descriptor)
	char* hash = calloc(2048 , sizeof(char));
    assert(hash != NULL);
	strcpy(hash,cl);
	strcat(hash,name);
	strcat(hash, descriptor);
	log("CREATE AND INSERT DHOOK INSERISCO HASH = %s\n", hash)
	//pthread_mutex_lock(&mutex);
	int i = inserisci(&L,dh,cl, name, descriptor, hash);
    //pthread_mutex_unlock(&mutex);
	if(debug)
		log("stampo lista: %d, dex_method = %s\n", i, dh->dex_meth)
	log("createAndInsertDhook Ho inserito: name = %s, mname = %s, sig = %s \n", dh->clname, dh->method_name, dh->method_sig)
	free(hash);
	free(cl);
	free(name);
	free(descriptor);
	//pthread_mutex_unlock(&cop);
	return EXIT_SUCCESS;
}


/**
	Wrapper di metodi JNI
*/
static jint _wrapper_dexstuff_loaddex(JNIEnv *env, char* dexpath){
	int mycookie  = NULL;
	//devo caricare un DEX esterno
	mycookie = dexstuff_loaddex(&d, dexpath);
	if(!mycookie){
		//error loaddex
		log("XXX7 ERROR LOADDEX\n")
		return 0;
	}
	else
		return mycookie;
}
static jobject _wrapper_dexstuff_defineclass(JNIEnv *env,int c, char* clsname){
	jclass *cls = (jclass*)dexstuff_defineclass(&d, clsname, c);
	if(!cls){
		//error defineclass
		return 0;
	}
	else
		return cls;
}

void * _wrapperSuspendAll(){
	log("XXX5 chiamato wrapper suspend\n")
	_suspendAllT(&d);
	//_dumpAllT(&d);
	log("XXX5 FINE wrapper suspend\n")
}
void * _wrapperResumeAll(){
	log("XXX5 chiamato wrapper resume\n")
	_resumeAllT(&d);
	//_dumpAllT(&d);
	log("XXX5 FINE wrapper resume\n")
}

//str deve essere dot o slash format senza L;
void* _dumpJavaClass(JNIEnv *env, jobject thiz, jobject str, jobject st2){
	//dalvik_dump_class(&d,"Ljavax/crypto/spec/PBEKeySpec;");
	char *clsname = (*env)->GetStringUTFChars(env, str, NULL);
	clsname = dvmDotToSlash(clsname);
	size_t len = strlen(clsname);
	char *clsnew = (char*)malloc(sizeof(char) * (len+3));
	clsnew[0]  = 'L';
	strcpy(clsnew+1, clsname);
	len = strlen(clsname);
	clsnew[len+1] = ';';
	clsnew[len+2] = '\0';
	struct ClassObject* res = (struct ClassObject*) _dvmFindLoadedClass(&d, clsnew);
	if(!res){
		log("XXX8 CLASSE NON TROVATA chiamo loadsystemclass con %s, str = %p\n", clsname, str)
		//res = _loadSystemClass(env, str);
		if(!res){
			;//res = (*env)->FindClass(env,clsname);
		}
	}
	else{
		//dalvik_dump_class(&d,"Ljavax/crypto/spec/PBEKeySpec;");
		log("XXX7 dumpjavaclass res = 0x%p, classloader = 0x%p\n", res, res->classLoader)
		log(" dvmdex = 0x%p\n",  res->pDvmDex)
		log("XXX7 cerco %s\n", clsnew)
		dalvik_dump_class(&d, clsnew);
		//_dvmFindStaticField(res, "z", "[Ljava/lang/String;");

		//res = (*env)->FindClass(env, "com/whatsapp/Voip");
		/**
		void* gres = (*env)->NewGlobalRef(env, res);
		 jfieldID v =  (*env)->GetStaticFieldID(env, gres, "z", "[Ljava/lang/String;");
		jobject vobj = (*env)->GetStaticObjectField(env, gres, v);
		 log("XXX7 preso static field\n" )
		 jmethodID mid = (*env)->GetStaticMethodID(env, gres, "<clinit>", "()V");
		 (*env)->CallStaticObjectMethod(env, gres, mid);
		log(" XXX7 chiamato clinit\n")
		*/
	}
	//diosolo(&d, env);
	free(clsnew);
	(*env)->ReleaseStringUTFChars(env, str, clsname); 
	// /startMagic(env);
}

static void _diosoloW(JNIEnv* env, jobject thiz){
	diosolo(&d,env);
}
static void _wrapperHandleAll(JNIEnv *env){
	handleAllMethodClass(&d,env);
}
/**
	Registro i metodi nativi che sono usati dal mondo  Java

JNINativeMethod method_table[] = {
    { "createStruct", "(Lorg/sid/addi/core/DalvikHook;)I",
        (void*) _createStruct },
    {"suspendALL","()V",
		(void*)_wrapperSuspendAll},
    {"resumeALL","()V",
		(void*)_wrapperResumeAll},
	{"unhook","(Ljava/lang/String;)V",
		(void*)_unhook},
	{"defineClass","(ILjava/lang/String;)I",
		(void*)_wrapper_dexstuff_defineclass},
	{"loadDex", "(Ljava/lang/String;)I",
		(void*)_wrapper_dexstuff_loaddex},
	{"startNewThread", "(Ljava/lang/String;Ljava/lang/String;)V",
		(void*)_newThread},
	{"dumpJavaClass", "(Ljava/lang/String;Ljava/lang/String;)V",
		(void*)_dumpJavaClass},
	{"diosoloW", "()V",
		(void*)_diosoloW},
	{ NULL, NULL, NULL },
};
*/
JNINativeMethod method_table[] = {
    { "createStruct", "(Lorg/sid/addi/core/DalvikHook;)I",
        (void*) createHookFromJava },
    {"suspendALL","()V",
		(void*)_wrapperSuspendAll},
    {"handleAllMethods","()V",
		(void*)_wrapperHandleAll},
    {"resumeALL","()V",
		(void*)_wrapperResumeAll},
//	{"disableHook","(Ljava/lang/String;)V",
//		(void*)_unhook},
    { NULL, NULL, NULL },
};


/* 
OLD STUFF

static void _register_api_native(JNIEnv *env, jclass mycls, jclass test){
	log("dentro register api, args: %p %p\n", mycls, test)
	(*env)->RegisterNatives(env, mycls, method_table, sizeof(method_table) / sizeof(method_table[0]));
	log("fine register api\n")
}


JNINativeMethod bootloader_table[] = {
    { "register_api_native", "(Ljava/lang/Class;)V",
        (void*) _register_api_native },
	{ NULL, NULL, NULL },
};
*/

/* 
	Funzioni ausiliarie per creare l'ambiente di esecuzione del framework
*/
int processIsZygote(pid_t p){
	char fname[256];
	sprintf(fname,"/proc/%d/cmdline",p);
	int fp = open(fname,O_RDONLY);
	if(fp<0){
		return 0;
	}
	read(fp, fname,sizeof(fname));
	close(fp);
	if(strstr(fname,"zygote")){
		return 1;
	}
	else
		return 0;
}

void createWorkingDir(pid_t p){
	struct stat st = {0};
	if(processIsZygote(p)){
		sprintf(working_dir, "/data/local/tmp/dynsec/zygote");
		if (stat(working_dir, &st) == -1) {
		   	mkdir(working_dir, S_IRWXU | S_IRWXG | S_IRWXO);
	   		//chmod(working_dir, j);
		}
		sprintf(logfile,"%s", working_dir);
		strcat(logfile,"/dynsec.log");
	}
	else{
		//sprintf(working_dir,"/data/local/tmp/dynsec/%d", p);
		sprintf(working_dir,"/data/local/tmp/dynsec");
		if(stat(working_dir, &st) == -1){
			mkdir(working_dir, S_IRWXU | S_IRWXG | S_IRWXO);
		}
		sprintf(working_dir,"/data/local/tmp/dynsec/%d", p);
		if (stat(working_dir, &st) == -1) {
		   	mkdir(working_dir, S_IRWXU | S_IRWXG | S_IRWXO);
	   		//chmod(working_dir, j);
		}
		sprintf(logfile,"%s", working_dir);
		strcat(logfile,"/dynsec.log");
		sprintf(stacklogfile,"%s", working_dir);
		strcat(stacklogfile,"/stacktraces.log");
	}
}

//metodo richiamato da Java per caricare un dex 
void* wrapperLoadDex(JNIEnv *env){

}
//metodo richiamato da Java per definire le classi
void* wrapperDefineClass(){

}


//quando definisco un nuovo hook a runtime dopo aver caricato il dex e definito le classi
//richiamo questo metodo per impostare gli hook
//l'impostazione avviene richiamando la createHookFromJava()
//clanme a/b/c
void* callPlaceHook(JNIEnv *env, char* clname, char* mname, char* msig){
	if(debug)
		log("placehook  su classe %s method: %s signature: %s\n", clname, mname, msig)
	jclass mycls = (*env)->FindClass(env, clname);
	if(debug)
		log("callplacehook trovata classe = %p \n", mycls)
	//jmethodID constructor = (*env)->GetStaticMethodID(env, mycls, "<init>", "()V");
	jmethodID place_hook = (*env)->GetStaticMethodID(env, mycls, mname, msig);
	//jobject obj = (*env)->NewObject(env, mycls, constructor);
	if(debug)
		log("callPLaceHook place_hook = %p \n", mycls, place_hook)
	(*env)->CallStaticVoidMethod(env,mycls,place_hook);
}
/**
	Funzione richiamata quando viene caricata la libreria dinamica in memoria
	Carica le classi Java che sono utilizzate dal framework
	e richiama la funzione del mondo Java che si occupa di impostare gli hook

	usando i metodi nativi per caricare le classi devo rispettare l'ordine, prima le superclassi poi le subclass...
	http://androidxref.com/4.4.2_r2/xref/dalvik/vm/oo/Class.cpp#dvmLinkClass
*/
jint my_ddi_init(){
	pthread_mutex_init(&mutex, NULL);	
	//not used
	pthread_mutex_init(&cop, NULL);
	assert(0==pthread_rwlockattr_init(&attr));
	pthread_rwlock_init(&_rwlock,&attr);
	//createWorkingDir(getpid());

	//initialize hashtable
	sm = sm_new(10);
	if (sm == NULL) {
    /* Handle allocation failure... */
		return EXIT_FAILURE;
	}

	if(debug)
		log("-------------------DENTRO  DDI INIT ---------------------\n")
	//dexstuff_resolv_dvm(&d);
	hackgDvm(&d);

	//int cookie2 = dexstuff_loaddex(&d, LOADER);
	cookie = dexstuff_loaddex(&d, APIFILE);
	int cookieHooks = dexstuff_loaddex(&d, "/data/local/tmp/classes.dex");
	log("cookie: %p, cookie2: %p\n", cookie, cookieHooks)
	
	//jclass *loader =(jclass*) dexstuff_defineclass(&d, "org/tesi/bootloader/BootLoader", cookie2);
	
	jclass *pipeServerCls =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/core/PipeServer", cookie);
	struct ClassObject* hookTCls = dexstuff_defineclass(&d, "org/sid/addi/core/HookT", cookie);	
	jclass *clazzHookType =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/core/HookT$HookType", cookie);
	jclass *strHelper =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/core/StringHelper", cookie);
	jclass *loggerWrap =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/core/LoggerWrap", cookie);

	jclass *dalvikHookCls =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/core/DalvikHook", cookie);
	jclass *manageADDICls =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/core/manageADDI", cookie);
	//PtyServer
	jclass *commonCls =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/core/Common", cookie);
	jclass *commandWrapCls =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/core/CommandWrapper", cookie);
	jclass *argsWrapCls =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/core/ArgumentWrapper", cookie);
	jclass *reqWrapCls =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/core/RequestWrapper", cookie);
	jclass *confWrapCls =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/core/ConfigWrapper", cookie);
	jclass *cmdClas =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/core/Commands", cookie);
	jclass *confCls =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/core/Config", cookie);
	jclass *xmlCls =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/core/XML", cookie);
	
	jclass *clazz5 = (jclass*) dexstuff_defineclass(&d, "org/sid/addi/core/AppState", cookie);

	jclass *clazz77 =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/logging/LoggerConfig", cookie);
	jclass *clazz88 =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/logging/LoggerTraces", cookie);
	jclass *clazz99 =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/logging/SQLiteHookDroidHelper", cookie);
    jclass *clazz58 =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/logging/SQLiteHookDroid", cookie);
    jclass *clazz59 =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/logging/SQLiteHookDroidLog", cookie);    
    jclass *clazz57 =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/logging/LoggerDB", cookie);
    jclass *clazz56 =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/logging/Logger", cookie);

	jclass *objWCls = (jclass*)dexstuff_defineclass(&d, "org/tesi/core/ObjWrapper", cookieHooks);
	//jclass *objWCls2 = (jclass*)dexstuff_defineclass(&d, "org/tesi/core/ObjWrapper$CreatorImplementation", cookieHooks);
	jclass *dalvikHImplCls =(jclass*) dexstuff_defineclass(&d, "org/tesi/core/DalvikHookImpl", cookieHooks);
    jclass *myInitCls = (jclass*)dexstuff_defineclass(&d, "org/tesi/core/MyInit", cookieHooks);
    jclass *logCls = (jclass*)dexstuff_defineclass(&d, "org/tesi/core/LoggingManager", cookieHooks);
	jclass *hooksListCls =(jclass*) dexstuff_defineclass(&d, "org/tesi/Hooks/HookList", cookieHooks);
	jclass *clazz55 =(jclass*) dexstuff_defineclass(&d, "org/tesi/utils/StringHelper", cookieHooks);
	jclass *networkHook =(jclass*) dexstuff_defineclass(&d, "org/tesi/Hooks/NetworkHooks", cookieHooks);
    jclass* appContext =(jclass*) dexstuff_defineclass(&d, "org/tesi/Hooks/AppContext", cookieHooks);
    jclass* IPCHookCls =(jclass*) dexstuff_defineclass(&d, "org/tesi/Hooks/IPCHooks", cookieHooks);
    jclass* sslHookCls =(jclass*) dexstuff_defineclass(&d, "org/tesi/Hooks/SSLHooks", cookieHooks);
    jclass* fsHookCls =(jclass*) dexstuff_defineclass(&d, "org/tesi/Hooks/FileSystemHooks", cookieHooks);
    jclass* sphookCls =(jclass*) dexstuff_defineclass(&d, "org/tesi/Hooks/SharedPrefHooks", cookieHooks);
    jclass* cryptohookCls =(jclass*) dexstuff_defineclass(&d, "org/tesi/Hooks/CryptoHooks", cookieHooks);
    jclass* hashhookCls =(jclass*) dexstuff_defineclass(&d, "org/tesi/Hooks/HashHooks", cookieHooks);
    jclass* reflectHookCls =(jclass*) dexstuff_defineclass(&d, "org/tesi/Hooks/ReflectHooks", cookieHooks);
    jclass* webVhookCls =(jclass*) dexstuff_defineclass(&d, "org/tesi/Hooks/WebviewHooks", cookieHooks);
    jclass* clsLoaderCls =(jclass*) dexstuff_defineclass(&d, "org/tesi/Hooks/classLoaderHooks", cookieHooks);

	JNIEnv *env = (JNIEnv*) get_jni_for_thread(&d);
	//dalvik_dump_class(&d, "Landroid/support/v7/app/ActionBarActivityDelegate;");
	/**
	struct ClassObject *pd = _dvmFindLoadedClass(&d,"Lorg/tesi/core/ObjWrapper;");
	struct ClassObject *pd2 = _dvmFindLoadedClass(&d,"Lorg/tesi/core/DalvikHookImpl;");
	struct ClassObject *pd3 = _dvmFindLoadedClass(&d,"Lorg/tesi/core/LoggingManager;");
	if(pd){
		log("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA trovata objwrapper\n")
		log("status wrapper: %d\n", pd->status)
		log("status dhook: %d\n", pd2->status)
		log("status log: %d\n", pd3->status)
		if(pd->status <= 5){
			//while(pd->status < CLASS_INITIALIZED){
			//	;
			//}
			//_dvmInitClass(&d,pd);
			log("BBBBBBBBBBBBBBBBBBBBBBBBBBBB init eseguito \n")
			log("status: %d\n", pd->status)
		}
	}
	struct Thread *self = getSelf(&d);
    if (d.dvmCheckException_fnPtr(self)) {
    	log("ERROR callOriginalV2 TROVATA EXCEPTION!!! \n")
    	Object* excep = d.dvmGetException_fnPtr(self);
    	if(excep != NULL){
            if(debug){
    		  log("XXX8 presa exception %p desc: %s\n", excep, excep->clazz->descriptor)
	    	  log("XXX111 lanciata except esco con -1\n")
            }
        }
        d.dvmClearException_fnPtr(self);
    } 
	*/
	if(debug)
		log("ddi XXX6 init preso env = 0x%p\n", env)	
	if(env){	
		struct Thread* self = getSelf(&d);
		/**
		Devo capire perchè non posso caricare tutto da java:
		- le API devono essere caricate nel system classloader
		- se il loader java usa new DexClassLoader() con il parent == system e carica 

		

		struct stat st = {0};
		if (stat("/data/local/tmp/odex", &st) == -1) {
	    	mkdir("/data/local/tmp/odex", S_IRWXU | S_IRWXG | S_IRWXO);
	    	log("CREATA DIR ODEX\n")
		}
		dalvik_dump_class(&d, "Lorg/tesi/bootloader/BootLoader;");
		jclass mycls = (*env)->FindClass(env, "org/tesi/bootloader/BootLoader");	
		jmethodID entrypoint = (*env)->GetStaticMethodID(env, mycls, "_entrypoint", "()V");
		(*env)->CallStaticVoidMethod(env,mycls,entrypoint);
		//dalvik_dump_class(&d, "Lorg/sid/addi/core/manageADDI;");
		//dalvik_dump_class(&d, "Lorg/sid/addi/core/DalvikHook;");
		//dalvik_dump_class(&d, "Lorg/tesi/core/DalvikHookImpl;");
		//dalvik_dump_class(&d, "Lorg/tesi/Hooks/AppContext;");
		//dalvik_dump_class(&d, "Lorg/tesi/Hooks/SharedPrefHooks;");
		(*env)->RegisterNatives(env, (*env)->FindClass(env, "org/sid/addi/core/manageADDI"), method_table, sizeof(method_table) / sizeof(method_table[0]));
		dalvikHookCls = (struct ClassObject*) _dvmFindLoadedClass(&d, DALVIKHOOKCLS_2);
		log("trovata dalvikhookcls: %p\n", dalvikHookCls)		
		jmethodID place_hook = (*env)->GetStaticMethodID(env, mycls, "_placehook", "()V");
		(*env)->CallStaticVoidMethod(env,mycls,place_hook);
		*/
		
		//shared by threads
		stackelem = (*env)->NewGlobalRef(env, (*env)->FindClass(env,"java/lang/StackTraceElement"));
		th = (*env)->NewGlobalRef(env,(*env)->FindClass(env, "java/lang/Thread"));
		(*env)->RegisterNatives(env, (*env)->FindClass(env, "org/sid/addi/core/manageADDI"), method_table, sizeof(method_table) / sizeof(method_table[0]));
		//load configuration
		//pConfigC = (*env)->FindClass(env, "org/sid/addi/core/Config");
		//jmethodID confInit = (*env)->GetStaticMethodID(env, pConfigC, "init", "()V");
		//(*env)->CallStaticVoidMethod(env,pConfigC, confInit);
		//log("XXX11 letta config\n")
		
		dalvikHookCls =  _dvmFindLoadedClass(&d, DALVIKHOOKCLS_2);
		/**
		struct ClassObject *porcodio = dalvikHookCls;
		if(debug)
			 log("dalvokhookcls status= %p\n", porcodio->status)
		//struct ClassObject *tmp = _dvmFindLoadedClass(&d, "Lorg/tesi/core/MyInit;");
		struct ClassObject *tmp = d.dvmFindClass_fnPtr("Lorg/tesi/core/MyInit;",NULL);

		if(tmp)
			if(debug)
				log("trovato Myinit %p, status = %d \n", tmp, tmp->status)
			if(tmp->status < 7 )
				_dvmInitClass(&d,tmp);
			if(debug)
				log("trovato Myinit %p, status = %d \n", tmp, tmp->status)
			struct Method *tmpm = d.dvmFindDirectMethodByDescriptor_fnPtr(tmp, "place_hook", "()V");
			if(tmpm && debug)
				log("trovato metodo place_hook %p\n", tmpm)
		
		if(self)
			if(debug)
				log("init: trovato self = %p, chiamo metodo %p nome: %s\n", self, tmpm, tmpm->name)
		JValue result;
		void* sobj = d.dvmCallMethodA_fnPtr(self, tmpm, NULL, false, &result, NULL );
			*/
		if(debug)
			log("trovato dhcls: %p , chiamo callPlaceHook\n", dalvikHookCls)
		callPlaceHook(env, "org/tesi/core/MyInit", "place_hook", "()V");
		//handleAllMethodClass(&d, env);
		/**
		jclass mycls = (*env)->FindClass(env, "org/tesi/core/MyInit");
		if(debug)
			log("XXX4 trovata classe myinit = 0x%p, dalvikhookcls: %p\n", mycls, dalvikHookCls)
		//jmethodID constructor = (*env)->GetStaticMethodID(env, mycls, "<init>", "()V");
		jmethodID place_hook = (*env)->GetStaticMethodID(env, mycls, "place_hook", "()V");
		//jobject obj = (*env)->NewObject(env, mycls, constructor);
		(*env)->CallStaticVoidMethod(env,mycls,place_hook);
		*/
	} 
	if(debug)
		log("------------------- FINE LOAD DEX INIT ---------------------\n")
	return JNI_OK;
}
static void* kingThread(){

    //call suspend all threads
    _suspendAllT(&d);
	my_ddi_init();
	lancia_consumer_producer();
	_createPTY();
	placeSupportHook();
	_resumeAllT(&d);
	//log("finito king \n")
	//detach from vm and exit thread
	return NULL;
}

void startKingThread(){
	dexstuff_resolv_dvm(&d);
	pthread_t king;
	//use dvmcreateinternalthread to create native thread
	d.dvmCreateInternalThread_fnPtr(&king, "KING", kingThread, NULL);
}

void _unhook(JNIEnv *env, jobject thiz, jobject str)
{
	log("XXX5 ------------ UNHOOK SONO STATO CHIAMATO -------- \n")

	char *c = (*env)->GetStringUTFChars(env, str, 0);
	log("XXX5 RICEVUTO = %s\n", c)
	struct dalvik_hook_t *res;
	pthread_mutex_lock(&mutex);
	res = (struct dalvik_hook_t *) cerca(L, c);
	
	if(!res){
		log("XXX5 errore RES = 0\n")
		return;
	}
	log("XXX5 trovato hook %s at 0x%p\n", res->clname, res)
	log("XXX5 trovato hook %s\n", res->method_name)
	res->type = UNUSED_HOOK;
	//dalvik_prepare(&d,res,env);
	//res->ok=0;
	pthread_mutex_unlock(&mutex);
	(*env)->DeleteLocalRef(env, str);
	log("XXX5 FINITO UNHOOK ------------------- \n")    
}

void* setJavaVM(JavaVM* yajvm){
	g_JavaVM = yajvm;
}


        /**
            struct ArrayObject* pAo = NULL;//d.dvmGetMethodThrows_fnPtr(original); //getmethodthrows se ne va a male
            log("XXX8 dopo dvmgetmethodthrows\n")
            if( pAo != NULL){
                log("XXX8 ARRAYOBJECT EXCEPTION NOT NULL! \n")
                log("diosolo arrayobj length = %d\n", pAo->length)
                struct Object** argsArray = (struct Object**)(void*)pAo->contents;
                size_t i = 0;
                char* c= NULL;
                for (i = 0; i < pAo->length; ++i) {
                    //log("pos[%d] = %s \n",i,argsArray[i]->clazz->descriptor);
                    //log("diosolo provo a ottenere char*\n")
                    c = d.dvmCreateCstrFromString_fnPtr((struct StringObject*)argsArray[i]);
                    log("diosolo [%d] preso char = %s\n", i, c)
                }
            }
            else{
                log("XXX8 ARRAYOBJECT EXCEPTION NULL! \n")
            }
            //void* str = d.dvmHumanReadableDescriptor_fnPtr(excep->clazz->descriptor);
            //log("XXX8 exception name: %s\n", (char*)str);
            
            //d.dvmWrapException_fnPtr("Ljava/lang/Exception;");
            //log("XXX8 dopo wrap exception \n")
            */
            /*
            if( strcmp(excep->clazz->descriptor, "Ljava/lang/ClassNotFoundException;") == 0){
                log("XXX8 lancio class not found exception\n")
                d.dvmThrowClassNotFoundException_fnPtr("Class Not found!!");
            }else if (strcmp(excep->clazz->descriptor, "Ljava/io/FileNotFoundException;") == 0){
                d.dvmThrowFileNotFoundException_fnPtr("aaaa");
            }
        
            else if(strcmp(excep->clazz->descriptor, "Ljava/lang/NullPointerException;") == 0){
                d.dvmThrowNullPointerException_fnPtr("Class Not found!!");
            }
            
            else if(strcmp(excep->clazz->descriptor, "Ljava/lang/IllegalArgumentException;") == 0){
                log("XXX11 lancio illegal argument excep \n")
                d.dvmThrowIllegalArgumentException_fnPtr("Class Not found!!");
            }
            else{
                ;//d.dvmClearException_fnPtr(self);
            }
            */






/*
int checkCaller(JNIEnv *env){
    
    jmethodID toS = (*env)->GetMethodID(env, stackelem, "toString", "()Ljava/lang/String;");
    
    //log("trovata clase thread: %p\n", th)
    jmethodID currid = (*env)->GetStaticMethodID(env, th, "currentThread", "()Ljava/lang/Thread;");
    //log("trovato mid: %p\n", currid)
    jobject cth = (*env)->CallStaticObjectMethod(env, th, currid);
    //log("trovata static m: %p\n", cth)
    jmethodID getST = (*env)->GetMethodID(env, th, "getStackTrace", "()[Ljava/lang/StackTraceElement;");
    //log("trovata m2: %p\n", getST)
    jobject arr = (*env)->CallObjectMethod(env, cth, getST);
    jsize len = (*env)->GetArrayLength(env, arr);
    int i = 0;
    //log("trovato array: %p, di len: %d\n", arr, len)
    jobject tmp;
    
    for(i=0; i < len; i++){
        tmp = (*env)->GetObjectArrayElement(env,arr,i);
        //log("preso elem[%d]: %p\n", i, tmp)
        jstring str = (*env)->CallObjectMethod(env, tmp, toS);
        //log("chiamo printstring con: %p\n", str)
        const char *buf = (*env)->GetStringUTFChars(env, str, 0);
        //log("preso buf: %s\n",buf)
        if(strstr(buf, HOOKCLASS2) != NULL || strstr(buf, APICLASS2) != NULL ){
            //log("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA CHIAMATO DA DIOOO\n")
            return 1;
        }
        (*env)->ReleaseStringUTFChars(env, str, buf);
        (*env)->DeleteLocalRef(env,tmp);
        (*env)->DeleteLocalRef(env,str);
        //printString(env, "valore: ", str);
    }
    return 0;
}*/

    
    /* new way
    //XXX
    struct ClassObject* pCo = dexstuff_defineclass(&d, "org/tesi/bootloader/BootLoader", cookie2);  
    struct ClassObject* pCo2 = dexstuff_defineclass(&d, "org/sid/addi/core/HookT", cookie); 
    jclass *clazzHookType =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/core/HookT$HookType", cookie);
    jclass *clazz55 =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/core/StringHelper", cookie);
    jclass *clazz44 =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/core/LogWrapper", cookie);    
    jclass *clazz22 =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/core/DalvikHook", cookie);
    jclass *clazzDEXHook =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/core/DEXHook", cookie);
    jclass *clazz33 =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/core/manageADDI", cookie);
    jclass *clazz60 =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/core/Common", cookie);
    jclass *clazz61 =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/core/CommandWrapper", cookie);
    jclass *clazz62 =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/core/ArgumentWrapper", cookie);
    jclass *clazz63 =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/core/RequestWrapper", cookie);
    jclass *clazz64 =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/core/Commands", cookie);
    jclass *clazz65 =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/core/Session", cookie);
    jclass *clazz66 =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/core/XML", cookie);
    jclass *clazz333 =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/core/manageADDI", cookie);
    jclass *clazz59 =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/Logs/LoggerConfig", cookie);
    jclass *clazz58 =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/Logs/LogTraces", cookie);
    jclass *clazz99 =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/Logs/LogToSQLiteHelper", cookie);
    jclass *clazz88 =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/Logs/LogToSQLiteClass", cookie);
    jclass *clazz77 =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/Logs/LogToSQLite", cookie);
    jclass *clazz57 =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/Logs/LogToDB", cookie);
    jclass *clazz56 =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/Logs/Logger", cookie);
    jclass *clazz5 = (jclass*) dexstuff_defineclass(&d, "org/sid/addi/utils/AppContextConfig", cookie);
    */
    /*
    struct ClassObject* pCo = dexstuff_defineclass(&d, "org/sid/addi/core/HookT", cookie);  
    jclass *clazzHookType =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/core/HookT$HookType", cookie);
    jclass *clazz55 =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/core/StringHelper", cookie);
    jclass *clazz44 =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/core/LogWrapper", cookie);    
    jclass *clazz22 =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/core/DalvikHook", cookie);
    jclass *clazzDEXHook =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/core/DEXHook", cookie);
    jclass *clazz59 =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/Logs/LoggerConfig", cookie);
    jclass *clazz58 =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/Logs/LogTraces", cookie);
    jclass *clazz99 =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/Logs/LogToSQLiteHelper", cookie);
    jclass *clazz88 =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/Logs/LogToSQLiteClass", cookie);
    jclass *clazz77 =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/Logs/LogToSQLite", cookie);
    jclass *clazz57 =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/Logs/LogToDB", cookie);
    jclass *clazz56 =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/Logs/Logger", cookie);
    jclass *clazz60 =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/core/Common", cookie);
    jclass *clazz61 =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/core/CommandWrapper", cookie);
    jclass *clazz62 =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/core/ArgumentWrapper", cookie);
    jclass *clazz63 =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/core/RequestWrapper", cookie);
    jclass *clazz64 =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/core/Commands", cookie);
    jclass *clazz65 =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/core/Session", cookie);
    jclass *clazz66 =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/core/XML", cookie);
    jclass *clazz5 = (jclass*) dexstuff_defineclass(&d, "org/sid/addi/utils/AppContextConfig", cookie);
    jclass *clazz33 =(jclass*) dexstuff_defineclass(&d, "org/sid/addi/core/manageADDI", cookie);
    // Define user hook
    jclass* billingHook =(jclass*) dexstuff_defineclass(&d, "org/tesi/Hooks/BillingHook", cookieHooks);
    jclass* LogHook =(jclass*) dexstuff_defineclass(&d, "org/tesi/Hooks/OnlyLogHook", cookieHooks);
    jclass* antiEmuHook =(jclass*) dexstuff_defineclass(&d, "org/tesi/Hooks/AntiEmulatorDetectionHook", cookieHooks);
    jclass* networkHook =(jclass*) dexstuff_defineclass(&d, "org/tesi/Hooks/NetworkHook", cookieHooks);
    jclass* IPCHook =(jclass*) dexstuff_defineclass(&d, "org/tesi/Hooks/IPCHooks", cookieHooks);
    jclass* appC =(jclass*) dexstuff_defineclass(&d, "org/tesi/utils/AppContextConfig", cookieHooks);
    jclass* webViewHook =(jclass*) dexstuff_defineclass(&d, "org/tesi/Hooks/WebviewHook", cookieHooks);
    jclass* sqliteHook =(jclass*) dexstuff_defineclass(&d, "org/tesi/Hooks/SQLiteHook", cookieHooks);
    jclass* sslhook =(jclass*) dexstuff_defineclass(&d, "org/tesi/Hooks/SSLHook", cookieHooks);
    jclass* fshook =(jclass*) dexstuff_defineclass(&d, "org/tesi/Hooks/FileSystemHook", cookieHooks);
    jclass* dummyhook =(jclass*) dexstuff_defineclass(&d, "org/tesi/Hooks/DummyHook", cookieHooks);
    jclass* sphook =(jclass*) dexstuff_defineclass(&d, "org/tesi/Hooks/SharedPrefHook", cookieHooks);
    jclass* cryptohook =(jclass*) dexstuff_defineclass(&d, "org/tesi/Hooks/CryptoHook", cookieHooks);
    jclass* hashhook =(jclass*) dexstuff_defineclass(&d, "org/tesi/Hooks/HashHook", cookieHooks);
    jclass* cryptokey =(jclass*) dexstuff_defineclass(&d, "org/tesi/Hooks/HookCryptoKey", cookieHooks);
    jclass *clazz7 =(jclass*) dexstuff_defineclass(&d, "org/tesi/core/DalvikHookImpl", cookieHooks);
    jclass *clazz3 =(jclass*) dexstuff_defineclass(&d, "org/tesi/Hooks/HookList", cookieHooks);
    jclass *testt =(jclass*) dexstuff_defineclass(&d, "org/tesi/Hooks/FuckMe", cookieHooks);
    jclass *clazz4 = (jclass*)dexstuff_defineclass(&d, "org/tesi/core/MyInit", cookieHooks);
    jclass *clazz6 = (jclass*) dexstuff_defineclass(&d, "org/tesi/utils/sendSMS", cookieHooks);
    */
