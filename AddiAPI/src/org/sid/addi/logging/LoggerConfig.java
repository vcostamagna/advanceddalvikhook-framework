package org.sid.addi.logging;

import org.sid.addi.core.DalvikHook;
import org.sid.addi.core.StringHelper;

public class LoggerConfig extends StringHelper{
	protected LoggerConfig() {
	}
	
	protected DalvikHook _config;
	
	public static String _TAG = "HookDroid";
	public static String _TAG_ERROR = "HookDroidError";
	public static String _TAG_LOG = "HookDroidLog";
	
	public static String getTag() {
		return _TAG;
	}
	
	public static String getTagError() {
		return _TAG_ERROR;
	}
	
	public static String getTagLog() {
		return _TAG_LOG;
	}

	protected String _out = "";
	protected String _notes = "";
	protected String _traces = "";
	
	protected boolean _enableDB = false;
	
	// this can be enabled via the _config file
	protected boolean _stackTraces = false;
	
	// change this value to get full traces
	protected boolean _fullTraces = false;
}
