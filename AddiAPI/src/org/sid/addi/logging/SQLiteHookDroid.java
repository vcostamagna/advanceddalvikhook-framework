package org.sid.addi.logging;

import java.util.ArrayList;
import java.util.List;

import org.sid.addi.core.AppState;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;


public class SQLiteHookDroid {
	private static SQLiteHookDroid _instance = null;
	// Database fields
	  private SQLiteDatabase database;
	  private SQLiteHookDroidHelper dbHelper;
	  private String[] allColumns = { 
			  SQLiteHookDroidHelper.COLUMN_ID,
			  SQLiteHookDroidHelper.COLUMN_TYPE,
			  SQLiteHookDroidHelper.COLUMN_SUBTYPE,
			  SQLiteHookDroidHelper.COLUMN_CLASS,
			  SQLiteHookDroidHelper.COLUMN_METHOD,
			  SQLiteHookDroidHelper.COLUMN_DETAILS,
			  SQLiteHookDroidHelper.COLUMN_LOG_TYPE,
			  SQLiteHookDroidHelper.COLUMN_NOTES,
			  SQLiteHookDroidHelper.COLUMN_ST
			  };
	
	  public SQLiteHookDroid(Context context) {
	    dbHelper = new SQLiteHookDroidHelper(context);
	  }
	
	  public void open() throws SQLException {
	    database = dbHelper.getWritableDatabase();
	  }
	
	  public void close() {
	    dbHelper.close();
	  }
	
	  public SQLiteHookDroidLog createRow(String type, String subType,
			  String className, String methodName, 
			  String details, String logType) {
		  return createRow(type, subType, className, 
				  methodName, details, logType, "", "");
	  }
	  
	  public SQLiteHookDroidLog createRow(String type, String subType,
			  String className, String methodName, 
			  String details, String logType, String notes) {
		  return createRow(type, subType, className, 
				  methodName, details, logType, notes, "");
	  }
	  
	  public SQLiteHookDroidLog createRow(String type, String subType,
			  String className, String methodName, String details,
			  String logType, String notes, String st) {
	    ContentValues values = new ContentValues();
	    values.put(SQLiteHookDroidHelper.COLUMN_TYPE, type);
	    values.put(SQLiteHookDroidHelper.COLUMN_SUBTYPE, subType); 
	    values.put(SQLiteHookDroidHelper.COLUMN_CLASS, className);
	    values.put(SQLiteHookDroidHelper.COLUMN_METHOD, methodName);
	    values.put(SQLiteHookDroidHelper.COLUMN_DETAILS, details);
	    values.put(SQLiteHookDroidHelper.COLUMN_LOG_TYPE, logType);
	    values.put(SQLiteHookDroidHelper.COLUMN_NOTES, notes);
	    values.put(SQLiteHookDroidHelper.COLUMN_ST, st);
	    
	    long insertId = database.insert(
	    		SQLiteHookDroidHelper.TABLE_TRACES, 
	    		null, values);
	    
	    Cursor cursor = database.query(
	    		SQLiteHookDroidHelper.TABLE_TRACES,
	    		allColumns, SQLiteHookDroidHelper.COLUMN_ID + 
	    		" = " + insertId, null,
	    		null, null, null);
	    
	    cursor.moveToFirst();
	    SQLiteHookDroidLog 
	    	newRow = cursorToRow(cursor);
	    cursor.close();
	    return newRow;
	  }
	
	  private SQLiteHookDroidLog cursorToRow(Cursor cursor) {
		  SQLiteHookDroidLog Row = 
				  new SQLiteHookDroidLog();
	    Row.setId(cursor.getLong(0));
	    Row.setRow(cursor.getString(1));
	    return Row;
	}
	
	public void deleteRow(SQLiteHookDroidLog Row) {
	    long id = Row.getId();
	    System.out.println("Row deleted with id: " + id);
	    database.delete(SQLiteHookDroidHelper.TABLE_TRACES, 
	    		SQLiteHookDroidHelper.COLUMN_ID
	    		+ " = " + id, null);
	  }
	
	  public List<SQLiteHookDroidLog> getAllRows() {
		    List<SQLiteHookDroidLog> Rows = 
		    		new ArrayList<SQLiteHookDroidLog>();
	
		    Cursor cursor = database.query(
		    	SQLiteHookDroidHelper.TABLE_TRACES,
		        allColumns, null, null, null, null, null);
	
		    cursor.moveToFirst();
		    while (!cursor.isAfterLast()) {
		    	SQLiteHookDroidLog Row = cursorToRow(cursor);
		      Rows.add(Row);
		      cursor.moveToNext();
		    }
		    // make sure to close the cursor
		    cursor.close();
		    return Rows;
		  }
	
	public static SQLiteHookDroid getInstance() {
		if (_instance  == null) {
			_instance = new 
					SQLiteHookDroid(AppState.getContext());
		}
		return _instance;
	}
}
