package org.sid.addi.logging;

import org.sid.addi.core.AppState;
import org.sid.addi.core.DalvikHook;

import android.util.Log;

public class Logger extends LoggerDB{
	private void clean() {
		_out = "";
		_pListArgsBody = "";
		_pListRetBody = "";
	}

	protected void _log(String data) {
		_out += data;
	}
	
	// ####### public

	public void logInit(DalvikHook config) {
		_config = config;
	}	
	
	public void logLine(String line) {
		_out += line + "\n";
	}
	
	public void logFlush_I(String notes) {
		_notes = notes;
		_out += notes;
		logFlush_I();
	}
	public void logFlush_W(String notes) {
		_notes = notes;
		_out += "-> !!! " + notes;
		logFlush_W();
	}
	
	// use a static ref to synchronize across threads
	public void logFlush_I() {
		_addTraces();
		synchronized (_TAG) {
			if (_enableDB)
				_logInDB("I");
			Log.i(_TAG, _out);
		}
		clean();
	}
	
	public void logFlush_W() {
		_addTraces();
		synchronized (_TAG) {
			if (_enableDB)
				_logInDB("W");
			Log.w(_TAG, _out);
		}
		clean();
	}
	
	public void logParameter(String name, String value) {
		if (_enableDB)
			_logDBParameter(name, value);
	}
	
	public void logParameter(String name, Object value) {
		if (_enableDB)
			_logDBParameter(name, "" + value);
	}
	
	public void logReturnValue(String name, String value) {
		if (_enableDB)
			_logDBReturnValue(name, value);
	}
	
	public void logReturnValue(String name, Object value) {
		if (_enableDB)
			_logDBReturnValue(name, "" + value);
	}
	
	public void logBasicInfo() {
		/**
		_log("### "+ _config.getType()+" ### " + 
				AppState.getPackageName() + 
				" - " + _config.get_clname() + "->" 
				+ _config.get_method_name() + "()\n");
				*/
	}
}
